<?php
	require('assets/config/config.php');
	//session_destroy();
?>
<html lang="en">
<head>
    <title>Home | E-Shopper</title>
    <link href="customer/css/bootstrap.min.css" rel="stylesheet">
    <link href="customer/css/font-awesome.min.css" rel="stylesheet">
    <link href="customer/css/prettyPhoto.css" rel="stylesheet">
    <link href="customer/css/price-range.css" rel="stylesheet">
    <link href="customer/css/animate.css" rel="stylesheet">
	<link href="customer/css/main.css" rel="stylesheet">
	<link href="customer/css/responsive.css" rel="stylesheet">      

</head><!--/head-->

<body>
	<header id="header"><!--header-->

		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><img src="customer/images/home/logo.png" alt="" height="100" width="200"/></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li class="active"><?php include ('customer/userlogin.php'); ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="customer/index.php">Home</a></li>
								<li><a href="customer/AllCategory.php">All Category</a></li> 
								<li><a href="customer/contactUs.php">Contact Us</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Login to your account</h2>
						<form method="post" action="#">
							<input type="text" name="username1" id="username1" placeholder="Your Username " />
							<input type="password" name="password1" id="password1" placeholder="Your Password" />
							<button type="submit" class="btn btn-default" name="submit">Login</button>
							<h5>Don't have an account? <a href="customer/register.php">Sign up</a></h5>
						</form>
					</div><!--/login form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	
	
	<footer id="footer"><!--Footer-->
		
		<div class="footer-widget">

		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2016.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href=""></a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer--> 
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>