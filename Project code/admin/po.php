<?php
	include('../assets/config/config.php');
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../login.php");
	}else{
		include 'interface/head.php';
?>
		<script>
		$(document).ready(function () {
			$('#datatable').dataTable({
				"language": {
					"decimal": ",",
					"thousands": ".",
					"lengthMenu": "Show _MENU_ supplier per page",
					"zeroRecords": "Nothing found",
					"info": "Showing _START_ to _END_ of _TOTAL_ supplier",
					"infoEmpty": "No records available",
					"infoFiltered": "(filtered from _MAX_ total records)"
				},
				"order": [[ 0, "asc" ]],
				"bLengthChange":false
				/*"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollCollapse": true,
				"autoWidth": false,
				"sScrollX": "100%",
				"sScrollX": "visible: false"*/
			});							
		});
		jQuery('#dataTable').wrap('<div style="overflow:auto;" />');
		
		function isNumberKey(evt)
		{
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
		}
		
		function isNumber(evt, element) {

			var charCode = (evt.which) ? evt.which : event.keyCode

			if (
				(charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
				(charCode < 48 || charCode > 57)){
				return false;
				}else{
				return true;
				}
		}
	</script>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
						<h1 class="page-head-line">Purchase Order</h1>
                    </div>
                </div>
				<?php
					if(isset($_POST['po'])){
						$company_name=$_POST['company_name'];
						$company_phone=$_POST['company_phone'];
						$name=$_POST['name'];
						$phone=$_POST['phone'];
						$address=$_POST['address'];
						$email=$_POST['email'];
						if(empty($company_name)||empty($company_phone)||empty($name)||empty($phone)||empty($address)||empty($email)){
							echo "<div class='alert alert-danger'>".
								 "Please fill out all fields.".
								"</div>";
						}else{
							$query=mysql_query("INSERT INTO purchase_order 
							VALUES('','$company_name','$company_phone','$name','$phone','$address','$email')");
							header("location:createPO.php");
						}
					}
				?>
                <div class="row">
                    <div class="col-md-12">	
                        <div class="row">
						<form id="form" method="post" action="#">
							<div class="panel panel-default">
								<div class="panel-heading">
									Purchase Form
								</div>
								<div class="panel-body">
									<label for="phone">Company Name:</label>
									<input type="text" name="company_name" class="form-control">
									<label for="phone">Company Phone:</label>
									<input type="text" name="company_phone" class="form-control" onkeypress="return isNumber(event)" maxlength="10">
									<label for="phone">Name:</label>
									<input type="text" name="name" class="form-control">
									<label for="phone">Phone Number:</label>
									<input type="text" name="phone" class="form-control" onkeypress="return isNumberKey(event)" maxlength="11">
									<label for="phone">Address:</label>
									<input type="text" name="address" class="form-control">
									<label for="phone">Email:</label>
									<input type="text" name="email" class="form-control">
									<input type="submit" style="margin:1% 0 0 0;" class="btn btn-info" name="po" value="Create Purchase Order">
								</div>
							</div>
						</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
	include 'interface/footer.php';
	}
?>