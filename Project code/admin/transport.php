<?php
	include('../assets/config/config.php');
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
	include 'interface/head.php';
?>
	<SCRIPT language=Javascript>
      <!--
		function isNumberKey(evt)
		{
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
		}
		
		function isNumber(evt, element) {

			var charCode = (evt.which) ? evt.which : event.keyCode

			if (
				(charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
				(charCode < 48 || charCode > 57)){
				return false;
				}else{
				return true;
				}
		}
      //-->
	</SCRIPT>	
	
	<div id="page-wrapper">
		<div id="page-inner">
			<div class="row">
				<div class="col-md-12">
					<h1 class="page-head-line">Transport</h1>
				</div>
			</div>
				<?php
					if(isset($_POST['insert'])){
						$fname=$_POST['fname'];
						$lname=$_POST['lname'];
						$ic=$_POST['ic'];
						$phone=$_POST['phone'];
						$gender=$_POST['gender'];
						$address1=$_POST['address1'];
						$address2=$_POST['address2'];
						$compname=$_POST['compname'];
						$compphone=$_POST['compphone'];
						$typeofvechicle=$_POST['typeofvechicle'];
						$numofplate=$_POST['numofplate'];
						$lvalidity=$_POST['lvalidity'];
						$destaddress1=$_POST['destaddress1'];
						$destaddress2=$_POST['destaddress2'];
						$issueperson=$_POST['issueperson'];
						$issuedate=$_POST['issuedate'];
						$issuetime=$_POST['issuetime'];
						$select_country=$_POST['select_country'];
						$mysql=mysql_query("INSERT INTO `transport`
						(`ID`, `fname`, `lname`, `ic`, `phone`, `gender`, `address1`, `address2`, `typeofvechicle`, `numofplate`,
						`lvalidity`, `destaddress1`, `destaddress2`, `issueperson`, `issuedate`, `issuetime`, `compname`,
						`compphone`, `country_iso`)
						VALUES 
						('','$fname','$lname','$ic','$phone','$gender','$address1','$address2','$typeofvechicle','$numofplate'
						,'$lvalidity','$destaddress1','$destaddress2','$issueperson','$issuedate','$issuetime','$compname',
						'$compphone','$select_country')");
						/*echo '<pre>';
						print_r($_POST);
						echo '</pre>';*/
					}
				?>
				<div class="row">
                    <div class="panel panel-default">
						<div class="panel-heading">
							Details 
						</div>
						<div class="panel-body">
						<form role="form" action="#" method="POST" enctype="multipart/form-data" >
						
							<div class="form-inline" style="margin:0 0 1% 0;">
								
								<label for="fname">First Name:</label>
								<input type="text" class="form-control" name="fname">
								<label for="lname">Last Name:</label>
								<input type="text" class="form-control" name="lname">
								<label for="ic">NRIC: </label>
								<input type="text" class="form-control" name="ic" maxlength="14" placeholder="E.g. 950101-01-6666" onkeypress="return isNumber(event)">
								
							</div>
							
							<div class="form-inline" style="margin:0 0 1% 0;">
								<label for="phone">Country:</label>
								<select class="form-control" name="select_country">
									<option selected="selected" value="sc">Select Country</option>
								<?php
									$q = mysql_query("SELECT * FROM country ORDER BY nicename ASC");
									while($row=mysql_fetch_assoc($q)){
								?>
									<option value="<?php echo $row['iso']; ?>"><?php echo $row['nicename']." &rlm;"."(&lrm;+".$row['phonecode'].")"; ?></option>
								<?php
									}
								?>
								</select>
							</div>
							
							<div class="form-inline" style="margin:0 0 1% 0;">
					
								<label for="phone">Phone:</label>
								&nbsp;&nbsp;
								<input type="text" style="width:6%;" class="form-control" onkeypress="return isNumberKey(event)" name="phone" maxlength="3">
								-
								<input type="text" style="width:9%;" class="form-control" onkeypress="return isNumberKey(event)" name="phone1" maxlength="7">
								&nbsp;&nbsp;
								<label for="gender">Gender :</label>
								<label class="radio-inline">
									<input type="radio" name="gender" value="Male" checked="">Male
								</label>
								
								<label class="radio-inline">
									<input type="radio" name="gender" value="Female">Female
								</label>
							
							</div>
							<div>
								
								<label for="address1">Address #1:</label>
								<input type="text" class="form-control" name="address1">
								<label for="address2">Address #2:</label>
								<input type="text" class="form-control" name="address2">
								
							</div>
							
							<div style="margin-bottom:1%;">
								<label for="compname">Company Name:</label>
								<input type="text" class="form-control" name="compname" >
								<label for="compphone">Company Phone:</label>
								<input type="tel" class="form-control" placeholder="00-0000000" name="compphone" maxlength="10">
							</div>
							
							<div class="form-inline" style="margin:0 0 1% 0;">
								
								<label for="typeofvechicle">Type of vehicle </label>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" class="form-control" name="typeofvechicle" >
								&nbsp;&nbsp;&nbsp;&nbsp;
								<label for="numofplate">Number of plate </label>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" class="form-control" name="numofplate" placeholder="xxx xxxx">
								
								<label for="lvalidity">License Validity</label>
								<input type="date" class="form-control" name="lvalidity">
								
							</div>
							</br>
							<div class="well">
							<div class="form-inline" style="margin:0 0 1% 0;">
								
								<h4><u><b>To destination </b></u></h4>
								<label for="destaddress1">Destination Address#1 </label>
								<input type="text" class="form-control" name="destaddress1">
								<label for="destaddress2">Destination Address #2 </label>
								<input type="text" class="form-control" name="destaddress2">
							</div>
							
							<div class="form-inline" style="margin:0 0 1% 0;">
								
								<label for="issueperson">Issue by </label>
								<input type="text" class="form-control" name="issueperson">
								<label for="issuedate">Issue Date </label>
								<input type="date" class="form-control" name="issuedate">
								<label for="issuetime">Issue Time </label>
								<input type="time" class="form-control" name="issuetime">
								
							</div>
							</div>
							
							<input type="submit" class="btn btn-info" name="insert" value="Add">
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
			
<?php
	include 'interface/footer.php';
	}
?>