<?php
	include('../assets/config/config.php');
	error_reporting(0);
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
	<script>
		$(document).ready(function () {
			$('#datatable').dataTable({
				"language": {
					"decimal": ",",
					"thousands": ".",
					"lengthMenu": "Show _MENU_ supplier per page",
					"zeroRecords": "Nothing found",
					"info": "",
					"infoEmpty": "No records available",
					"infoFiltered": "(filtered from _MAX_ total records)"
				},
				"ordering": false,
				"bFilter":false,
				"paging":   false,
				//"order": [[ 0, "asc" ]],
				"bLengthChange":false,
				//"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollCollapse": true,
				"autoWidth": false,
				"sScrollX": "100%",
				"sScrollX": "visible: false"
			});							
		});
		jQuery('#dataTable').wrap('<div style="overflow:auto;" />');
	</script>

	
	<div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
					<h1 class="page-head-line">Create Purchase Order</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-md-12">	
                    <div class="row">
					<form method="post" action="createPO.php">
					<div class="panel panel-default">
						<div class="panel-heading">
							Purchase Order Form
						</div>
						<div class="panel-body">
						<?php
							if(isset($_POST['insert'])){
								$count = count($_POST['select_item']);
								//echo $count;
								$id=$_POST['id'];
								$name=$_POST['name'];
								$compn=$_POST['compn'];
								$cphone=$_POST['cphone'];
								$phone=$_POST['phone'];
								$address=$_POST['address'];
								$email=$_POST['email'];
								$date=$_POST['date'];
								$poid=$_POST['poid'];
								
								for($i=0;$i<$count;$i++){
									$item_name=$_POST['select_item'][$i];
									$quantity=$_POST['quantity'][$i];
									$unit_price=$_POST['unit_price'][$i];
									$total_price=$_POST['total'][$i];
									if(!empty($item_name)&&!empty($quantity)&&!empty($unit_price)&&!empty($total_price)&&$item_name!='si'){
										$query3=mysql_query("INSERT INTO purchase_order_details VALUES('','$poid','$item_name','$quantity','$quantity','$unit_price','$total_price')");
										//header("location:podetails.php");
									}
								}
								//echo '<pre>'; print_r($_POST); echo '</pre>';
									if(!$query3){
										echo "<div class='alert alert-danger'>".
											"Please fill out all fields.".
											"</div>";
									}else{
										$insert=mysql_query("INSERT INTO purchase_order VALUES('','$poid',now(),'$id')");
										echo "<div class='alert alert-info'>".
											"Purchase order create successfully.".
											"</div>";
									}
							}
							if(isset($_GET['id'])){
								$ID=$_GET['id'];
								$query2=mysql_query("SELECT * FROM supplier WHERE ID=$ID")or die(mysql_error());
								while($row = mysql_fetch_assoc($query2)){
									$id=$row['ID'];
									$name=$row['person_in_charge'];
									$compn=$row['company_name'];
									$phone=$row['phone'];
									$cphone=$row['company_phone'];
									$address=$row['address'];
									$email=$row['email'];
								}
							}
							date_default_timezone_set("Asia/Kuala_Lumpur"); 
							
						?>
						<div class="col-md-6">
							<label><u>Supplier Detail</u></label>
							<table class="adjusttd">
								<tr>
									<td>Supplier ID:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $id; ?>" name="id"><?php echo "S".$id; ?></td>
								</tr>
								<tr>
									<td>Name:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $name; ?>" name="name"><?php echo $name; ?></td>
								</tr>
								<tr>
									<td>Phone Number:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $phone; ?>" name="phone"><?php echo $phone; ?></td>
								</tr>
								<tr>
									<td>Company:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $compn; ?>" name="compn"><?php echo $compn; ?></td>
								</tr>
								<tr>
									<td>Company P/H:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $cphone; ?>" name="cphone"><?php echo $cphone; ?></td>
								</tr>
								<tr>
									<td>Email:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $email; ?>" name="email"><?php echo $email; ?></td>
								</tr>
								<tr>
									<td>Address:</td>
									<td style="width:5px;"><input type="hidden" value="<?php echo $address; ?>" name="address">
									<?php 
										//echo $address;
										$addr=explode("|",$address,2);
										$count=count($addr);
										echo $addr[0].", ".$addr[1]
									?>
									</td>
								</tr>
							</table>
						</div>
						<div class="col-md-6">
							<?php
							$query4=mysql_query("SELECT * FROM purchase_order");
							$countall=mysql_num_rows($query4);
							$poid=$countall+1;
							$padded = str_pad((string)$poid, 5, "0", STR_PAD_LEFT); //auto fill in zero
							?>
							<label><u>Purchase Order Detail</u></label>
							<table class="adjusttd">
								<tr>
									<td>Purchase Order No:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $padded; ?>" name="poid"><?php echo "P".$padded; ?></td>
								</tr>
								<tr>
									<td>Date:&nbsp;</td>
									<td><input type="hidden" value="<?php echo date("Y-m-d H:i:s"); ?>" name="date"><?php echo  date("d/m/Y"); ?></td>
								</tr>
							</table>
						</div>
						</div>
						
						<input class="btn btn-info" type="button" style="margin:0 0 0 1%;" value="Add Row" onclick="addRow('datatable'); gg();">
<!--------------------------------------------------PO Table------------------------------------------------>
	<script>
		$(".select_item").ready(function(){
			gg();
			
		})
		function addRow(tableID) {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;
			if(rowCount <= 15){							// limit the user from creating fields more than your limits
				var row = table.insertRow(rowCount);
				var colCount = table.rows[1].cells.length;
				for(var i=0; i<colCount; i++) {
					var newcell = row.insertCell(i);
					newcell.innerHTML = table.rows[1].cells[i].innerHTML;
					console.log(table.rows[1].cells[i].innerHTML);
				}
			}else{
				 alert("Maximum Passenger per ticket is 12.");
					   
			}
		}
		
		function isNumberKey(evt)
		{
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
		}

		/*$('#select_item').on('change', function(){
			var m = (this.value);
			var n = $('.price'+m).attr('role');
			$('#price[]').val(n);
			return false;
		});*/
		function caltotal(td){
			var row=$(document).find(td).parents("tr");
			var quantity=row.children("td:nth-child(2)").children("#quantity").val();		
			var price=Number(row.children("td:nth-child(3)").children("#price").val().replace(/[^0-9\.]+/g,""));
			var total = parseFloat(quantity)*parseFloat(price);
			//var sum = 0;
			if(quantity>0){
				row.children("td:nth-child(4)").children("#total").val(parseFloat(total).toFixed(2));
				//sum += Number(parseFloat(total).toFixed(2));
			}else{
				row.children("td:nth-child(4)").children("#total").val(parseFloat(0).toFixed(2));
				//sum += Number(parseFloat(0).toFixed(2));
			}
		}
		
		$(document).ready(function(){
			//iterate through each textboxes and add keyup
			//handler to trigger sum event
			$(".form-control").each(function() {

				$(this).keyup(function(){
					calculateSum();
				});
			});

		});

		function calculateSum() {
			var sum = 0;
			//iterate through each textboxes and add the values
			$(".total").each(function() {

				//add only if the value is number
				if(!isNaN(this.value) && this.value.length!=0) {
					sum += parseFloat(this.value);
				}

			});
			//.toFixed() method will roundoff the final sum to 2 decimal places
			$("#total_cost").html(sum.toFixed(2));
		}
		
		function gg() {
			$(".select_item").change(function () {
				var selectedText = $(this).find("option:selected").text();
				var selectedValue = $(this).children(':selected').attr('role');
				var x = document.getElementById("datatable").rows.length;
				var price = $(this).parents("tr").children("td:nth-child(3)");
				
				price.children("#price").val(selectedValue);
				caltotal($(this));
				/*--------------select option option selected disabled the option--------------------*/
				 $('select option').attr('disabled',false);
    
				// loop each select and set the selected value to disabled in all other selects
				$('select').each(function(){
					var $this = $(this);
					$('select').not($this).find('option').each(function(){
					   if($(this).attr('value') == $this.val())
						   $(this).attr('disabled',true);
					});
				});
				
			});
		};
	</script>
						<div class="panel-body">
							<div class="table-responsive">
								<table id="datatable" class="display cell-border table-bordered table-striped" style="border-bottom:1px solid #ddd;">
									<thead>
										<tr>
											<th>Item</th>
											<th>Quantity</th>
											<th>Unit Price (RM)</th>
											<th>Total (RM)</th>
										</tr>
									</thead>
									<tbody>
									<tr>
										<td>
											<select class="form-control select_item" name="select_item[]" id="select_item">
											<option selected="selected"></option>
											<?php
												$q = mysql_query("SELECT * FROM stock ORDER BY name ASC");
												while($row2=mysql_fetch_assoc($q)){
											?>
											<option class="price<?php echo $row2['name']; ?>" value="<?php echo $row2['name']; ?>" role="<?php echo number_format($row2['companyPrice'],2); ?>"><?php echo $row2['name'];?></option>
											<?php
												}
											?>
											</select>
										</td>
										<td><input type="number" id="quantity" name="quantity[]" onKeyUp="caltotal(this);myFunction();" class="form-control" min="1"></td>
										<td class="price">
											<!--<input type="textbox" id="price" class="form-control" disabled>-->
											<input type="textbox" id="price" name="unit_price[]" class="form-control" readonly>
										</td>
										<td>
											<!--<input type="textbox" id="total" class="form-control" disabled>-->
											<input type="textbox" id="total" name="total[]" class="form-control total" readonly>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control select_item" name="select_item[]" id="select_item">
											<option selected="selected"></option>
											<?php
												$q = mysql_query("SELECT * FROM stock ORDER BY name ASC");
												while($row2=mysql_fetch_assoc($q)){
											?>
											<option class="price<?php echo $row2['name']; ?>" value="<?php echo $row2['name']; ?>" role="<?php echo number_format($row2['companyPrice'],2); ?>"><?php echo $row2['name'];?></option>
											<?php
												}
											?>
											</select>
										</td>
										<td><input type="number" id="quantity" name="quantity[]" onKeyUp="caltotal(this);myFunction();" class="form-control" min="1"></td>
										<td class="price">
											<!--<input type="textbox" id="price" class="form-control" disabled>-->
											<input type="textbox" id="price" name="unit_price[]" class="form-control" readonly>
										</td>
										<td>
											<!--<input type="textbox" id="total" class="form-control" disabled>-->
											<input type="textbox" id="total" name="total[]" class="form-control total" readonly>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control select_item" name="select_item[]" id="select_item">
											<option selected="selected"></option>
											<?php
												$q = mysql_query("SELECT * FROM stock ORDER BY name ASC");
												while($row2=mysql_fetch_assoc($q)){
											?>
											<option class="price<?php echo $row2['name']; ?>" value="<?php echo $row2['name']; ?>" role="<?php echo number_format($row2['companyPrice'],2); ?>"><?php echo $row2['name'];?></option>
											<?php
												}
											?>
											</select>
										</td>
										<td><input type="number" id="quantity" name="quantity[]" onKeyUp="caltotal(this);myFunction();" class="form-control" min="1"></td>
										<td class="price">
											<!--<input type="textbox" id="price" class="form-control" disabled>-->
											<input type="textbox" id="price" name="unit_price[]" class="form-control" readonly>
										</td>
										<td>
											<!--<input type="textbox" id="total" class="form-control" disabled>-->
											<input type="textbox" id="total" name="total[]" class="form-control total" readonly>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control select_item" name="select_item[]" id="select_item">
											<option selected="selected"></option>
											<?php
												$q = mysql_query("SELECT * FROM stock ORDER BY name ASC");
												while($row2=mysql_fetch_assoc($q)){
											?>
											<option class="price<?php echo $row2['name']; ?>" value="<?php echo $row2['name']; ?>" role="<?php echo number_format($row2['companyPrice'],2); ?>"><?php echo $row2['name'];?></option>
											<?php
												}
											?>
											</select>
										</td>
										<td><input type="number" id="quantity" name="quantity[]" onKeyUp="caltotal(this);myFunction();" class="form-control" min="1"></td>
										<td class="price">
											<!--<input type="textbox" id="price" class="form-control" disabled>-->
											<input type="textbox" id="price" name="unit_price[]" class="form-control" readonly>
										</td>
										<td>
											<!--<input type="textbox" id="total" class="form-control" disabled>-->
											<input type="textbox" id="total" name="total[]" class="form-control total" readonly>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control select_item" name="select_item[]" id="select_item">
											<option selected="selected"></option>
											<?php
												$q = mysql_query("SELECT * FROM stock ORDER BY name ASC");
												while($row2=mysql_fetch_assoc($q)){
											?>
											<option class="price<?php echo $row2['name']; ?>" value="<?php echo $row2['name']; ?>" role="<?php echo number_format($row2['companyPrice'],2); ?>"><?php echo $row2['name'];?></option>
											<?php
												}
											?>
											</select>
										</td>
										<td><input type="number" id="quantity" name="quantity[]" onKeyUp="caltotal(this);myFunction();" class="form-control" min="1"></td>
										<td class="price">
											<!--<input type="textbox" id="price" class="form-control" disabled>-->
											<input type="textbox" id="price" name="unit_price[]" class="form-control" readonly>
										</td>
										<td>
											<!--<input type="textbox" id="total" class="form-control" disabled>-->
											<input type="textbox" id="total" name="total[]" class="form-control total" readonly>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control select_item" name="select_item[]" id="select_item">
											<option selected="selected"></option>
											<?php
												$q = mysql_query("SELECT * FROM stock ORDER BY name ASC");
												while($row2=mysql_fetch_assoc($q)){
											?>
											<option class="price<?php echo $row2['name']; ?>" value="<?php echo $row2['name']; ?>" role="<?php echo number_format($row2['companyPrice'],2); ?>"><?php echo $row2['name'];?></option>
											<?php
												}
											?>
											</select>
										</td>
										<td><input type="number" id="quantity" name="quantity[]" onKeyUp="caltotal(this);myFunction();" class="form-control" min="1"></td>
										<td class="price">
											<!--<input type="textbox" id="price" class="form-control" disabled>-->
											<input type="textbox" id="price" name="unit_price[]" class="form-control" readonly>
										</td>
										<td>
											<!--<input type="textbox" id="total" class="form-control" disabled>-->
											<input type="textbox" id="total" name="total[]" class="form-control total" readonly>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control select_item" name="select_item[]" id="select_item">
											<option selected="selected"></option>
											<?php
												$q = mysql_query("SELECT * FROM stock ORDER BY name ASC");
												while($row2=mysql_fetch_assoc($q)){
											?>
											<option class="price<?php echo $row2['name']; ?>" value="<?php echo $row2['name']; ?>" role="<?php echo number_format($row2['companyPrice'],2); ?>"><?php echo $row2['name'];?></option>
											<?php
												}
											?>
											</select>
										</td>
										<td><input type="number" id="quantity" name="quantity[]" onKeyUp="caltotal(this);myFunction();" class="form-control" min="1"></td>
										<td class="price">
											<!--<input type="textbox" id="price" class="form-control" disabled>-->
											<input type="textbox" id="price" name="unit_price[]" class="form-control" readonly>
										</td>
										<td>
											<!--<input type="textbox" id="total" class="form-control" disabled>-->
											<input type="textbox" id="total" name="total[]" class="form-control total" readonly>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control select_item" name="select_item[]" id="select_item">
											<option selected="selected"></option>
											<?php
												$q = mysql_query("SELECT * FROM stock ORDER BY name ASC");
												while($row2=mysql_fetch_assoc($q)){
											?>
											<option class="price<?php echo $row2['name']; ?>" value="<?php echo $row2['name']; ?>" role="<?php echo number_format($row2['companyPrice'],2); ?>"><?php echo $row2['name'];?></option>
											<?php
												}
											?>
											</select>
										</td>
										<td><input type="number" id="quantity" name="quantity[]" onKeyUp="caltotal(this);myFunction();" class="form-control" min="1"></td>
										<td class="price">
											<!--<input type="textbox" id="price" class="form-control" disabled>-->
											<input type="textbox" id="price" name="unit_price[]" class="form-control" readonly>
										</td>
										<td>
											<!--<input type="textbox" id="total" class="form-control" disabled>-->
											<input type="textbox" id="total" name="total[]" class="form-control total" readonly>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control select_item" name="select_item[]" id="select_item">
											<option selected="selected"></option>
											<?php
												$q = mysql_query("SELECT * FROM stock ORDER BY name ASC");
												while($row2=mysql_fetch_assoc($q)){
											?>
											<option class="price<?php echo $row2['name']; ?>" value="<?php echo $row2['name']; ?>" role="<?php echo number_format($row2['companyPrice'],2); ?>"><?php echo $row2['name'];?></option>
											<?php
												}
											?>
											</select>
										</td>
										<td><input type="number" id="quantity" name="quantity[]" onKeyUp="caltotal(this);myFunction();" class="form-control" min="1"></td>
										<td class="price">
											<!--<input type="textbox" id="price" class="form-control" disabled>-->
											<input type="textbox" id="price" name="unit_price[]" class="form-control" readonly>
										</td>
										<td>
											<!--<input type="textbox" id="total" class="form-control" disabled>-->
											<input type="textbox" id="total" name="total[]" class="form-control total" readonly>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control select_item" name="select_item[]" id="select_item">
											<option selected="selected"></option>
											<?php
												$q = mysql_query("SELECT * FROM stock ORDER BY name ASC");
												while($row2=mysql_fetch_assoc($q)){
											?>
											<option class="price<?php echo $row2['name']; ?>" value="<?php echo $row2['name']; ?>" role="<?php echo number_format($row2['companyPrice'],2); ?>"><?php echo $row2['name'];?></option>
											<?php
												}
											?>
											</select>
										</td>
										<td><input type="number" id="quantity" name="quantity[]" onKeyUp="caltotal(this);myFunction();" class="form-control" min="1"></td>
										<td class="price">
											<!--<input type="textbox" id="price" class="form-control" disabled>-->
											<input type="textbox" id="price" name="unit_price[]" class="form-control" readonly>
										</td>
										<td>
											<!--<input type="textbox" id="total" class="form-control" disabled>-->
											<input type="textbox" id="total" name="total[]" class="form-control total" readonly>
										</td>
									</tr>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="3"><b>Total Cost</b></td>
											<td><span id="total_cost"></span></td>
										</tr>
									</tfoot>
								</table>	
								<input type="submit" class="btn btn-info" name="insert" value="Submit">
							</div>
						</div>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
<?php
	include 'interface/footer.php';
	}
?>
