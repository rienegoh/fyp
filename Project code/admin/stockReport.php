<?php
	include('../assets/config/config.php');
	
	error_reporting(0);
	
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		
		/*---------------------------------Checkbox Delete---------------------------------------------*/
		if($_POST['submit']='delete'){
			$rowCount = count($_POST["checkbox"]);
			//$_POST[""];
			for($i=0;$i<$rowCount;$i++) {
				$d = mysql_query("DELETE FROM stock WHERE ID='" . $_POST["checkbox"][$i] . "'");
				$delete_check = true;
			}
			/*echo '<pre>';
			print_r($_POST);
			echo '</pre>';*/
		}
		/*---------------------------------Checkbox Delete End---------------------------------------------*/
		
		/*---------------------------------------Update---------------------------------------------------*/
		if(isset($_POST['update'])){
			$id=$_POST['id'];
			$category_prefix=$_POST['category_prefix'];
			$name=$_POST['name'];
			$quantity=$_POST['quantity'];
			$cp=$_POST['cp'];
			$sp=$_POST['sp'];
			$category=$_POST['category'];
			$img=$_FILES['images']['name'];
			$img_tmp=$_FILES['images']['tmp_name'];
			$description=$_POST['description'];
			$date=$_POST['date'];
			if($img){
				if(($_FILES['images']['type'][$i]=='image/gif')
				|| ($_FILES['images']['type'][$i]=='image/jpeg')
				|| ($_FILES['images']['type'][$i]=='image/pjpeg')
				|| ($_FILES['images']['size'][$i]<200000)){
				move_uploaded_file($img_tmp,"../assets/images/".$img);
				$query3 = mysql_query("UPDATE stock SET ID='$id', category_prefix='$category', name='$name', companyPrice='$cp', sellingPrice='$sp', image='$img',image_data='$img_tmp', date=CURDATE(),description='$description' WHERE ID='$id' AND category_prefix='$category_prefix'");
				}else{
					$image_type=true;
				}
			}else{
			$query3 = mysql_query("UPDATE stock SET ID='$id', category_prefix='$category', name='$name', companyPrice='$cp', sellingPrice='$sp', date=CURDATE(),description='$description' WHERE ID='$id' AND category_prefix='$category_prefix'");
			}
		}
		/*---------------------------------------Update End---------------------------------------------*/
		include 'interface/head.php';			
?>
	<script>
		$(document).ready(function () {
			$('#datatable').dataTable({
				"language": {
					"decimal": ",",
					"thousands": ".",
					"lengthMenu": "Show _MENU_ item per page",
					"zeroRecords": "Nothing found",
					"info": "Showing _START_ to _END_ of _TOTAL_ items",
					"infoEmpty": "No records available",
					"infoFiltered": "(filtered from _MAX_ total records)"
				},
				"order": [[ 0, "asc" ]],
				//"bLengthChange":false,
				//"pageLength": 12
				/*"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollY":        "800px",
				"scrollCollapse": true,
				"autoWidth": false,
				"sScrollX": "100%",
				"sScrollX": "visible: false",
				"aoColumnDefs": [
					{ "sWidth": "10%", "aTargets": [ -1 ] }
				]*/
			});							
		});
		jQuery('#dataTable').wrap('<div style="overflow:auto;" />');
	</script>
	<div id="page-wrapper">
        <div id="page-inner">
			<?php
				$action = isset($_GET['action']) ? $_GET['action'] : "";
				$name = isset($_GET['name']) ? $_GET['name'] : "";
				$id = isset($_GET['id']) ? $_GET['id'] : "";
				$category_prefix = isset($_GET['category_prefix']) ? $_GET['category_prefix'] : "";
					 
				if($delete_check==true){
					echo "<div class='alert alert-info'>";
					echo "Delete Successful!";
					echo "</div>";
				}else if($query3){
					echo "<div class='alert alert-info'>";
					echo "<strong><i>{$category_prefix}{$id}</i> {$name}</strong> detail was updated!";
					echo "</div>";
				}
			?>
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">Inventory Manage</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-md-12">
                    <div class="row">
						<form id="form" method="post" action="#">
						<div class="panel panel-default">
							<div class="panel-heading">
								Inventory Detail
							</div>
							<div class="panel-body">
								<div class="table-responsive" style="overflow:auto;">
									<table id="datatable" class="display cell-border table-bordered table-striped" style="width:100%;border-bottom:1px solid #ddd;">
										<thead>
											<tr>
												<th style="width:4%;">
													<div class="checkbox">
														<input type="checkbox" id="checkall" onchange="checkall()">
													</div>
												</th>
												<th>ID</th>
												<th>Name</th>
												<th>In Stock</th>
												
											</tr>
										</thead>
										<tbody>
										<?php
											$q=mysql_query("SELECT * FROM stock");
											while($row3=mysql_fetch_assoc($q)){
										?>
											<tr>
												<td>
												<center>
												<div class="checkbox">
													<input type="checkbox" id="checkbox[]" class="group" name="checkbox[]" value="<?php echo $row3['ID']; ?>">
												</div>
												</center>
												</td>
												<td><?php echo $row3['ID']; ?></td>
												<td><?php echo $row3['name']; ?></td>
												<td><?php echo $row3['quantity']; ?></td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
									<!--Delete All-->
									<button type="submit" name="delete" title="delete" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?');" value="delete">
									<i class="fa fa-trash"></i> Delete Selected
									</button>
								</div>
							</div>	
						</div>		
						</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<?php
	include 'interface/footer.php';
	}
?>