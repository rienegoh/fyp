<?php
	include('../assets/config/config.php');
	
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
	<SCRIPT language=Javascript>
      <!--
		function isNumberKey(evt)
		{
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
		}
		
		function isNumber(evt, element) {

			var charCode = (evt.which) ? evt.which : event.keyCode

			if (
				(charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
				(charCode < 48 || charCode > 57)){
				return false;
				}else{
				return true;
				}
		}
      //-->
	</SCRIPT>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Edit Admin</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
							<div class="panel panel-default">
								<div class="panel-heading">
									Admin Detail
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<?php
											$id="";
											$username="";
											$password="";
											$name="";
											$email="";
											$dob="";
											$ic="";
											$phone="";
											$gender="";
											$addr="";
											if(isset($_GET['edit'])){
												
												$id=filter_var($_GET['edit'],FILTER_SANITIZE_NUMBER_INT);
												$query2=mysql_query("SELECT * FROM admin WHERE ID=$id")or die(mysql_error());
												
												while ($row2=mysql_fetch_assoc($query2)){
													$id=$row2['ID'];
													$username=$row2['username'];
													$password=$row2['password'];
													$fname=$row2['firstName'];
													$lname=$row2['lastName'];
													$email=$row2['email'];
													$dob=$row2['dob'];
													//$ic=$row2['ic'];
													$gender=$row2['gender'];
													$address=$row2['address'];
													/*----IC----*/
													$ic=explode('-', $row2['ic'], 3);
													$countic=count($ic);
													/*----phone----*/
													//$phone=$row2['phone'];
													$phone=explode('-', $row2['phone'], 2);
													$countph=count($phone);
													//$type=$row2['type'];
													/*----Address----*/
													$addr=explode('|', $row2['address'], 2);
													$count=count($addr);
												}
											}
										?>
											<div class="col-md-6">
										<form role="form" action="admin.php?action=update&id=<?php echo $id;?>&name=<?php echo $fname." ".$lname;?>" method="POST">
											<div>
												<input type="hidden" name="id" value="<?php echo $id; ?>">
												<label for="name">Name:</label>
												<div class="input-group">
													<span class="input-group-addon">First Name</span>
													<input type="text" class="form-control" name="fname" value="<?php echo $fname; ?>" style="z-index:0;">
													<span class="input-group-addon">Last Name</span>
													<input type="text" class="form-control" name="lname" value="<?php echo $lname; ?>" style="z-index:0;">
												</div>
											</div>
											<div>
												<label for="ic">I/C No: </label>
												<div class="input-group">
												<input type="text" class="form-control" name="ic" value="<?php if($countic>0){echo $ic[0];} ?>" maxlength="6" onkeypress="return isNumber(event)" style="z-index:0;">
													<span class="input-group-addon">-</span>
												<input type="text" class="form-control" name="ic1" value="<?php if($countic>1){echo $ic[1];} ?>" maxlength="2" onkeypress="return isNumber(event)" style="z-index:0;">
													<span class="input-group-addon">-</span>
												<input type="text" class="form-control" name="ic2" value="<?php if($countic>2){echo $ic[2];} ?>" maxlength="4" onkeypress="return isNumber(event)" style="z-index:0;">
												</div>
											</div>
											<div>
												<label for="fname">Username:</label>
												<input type="text" class="form-control" name="username" value="<?php echo $username; ?>" disabled>
											</div>
											<div>
												<label for="fname">Password:</label>
												<input type="password" class="form-control" name="password" value="<?php echo $password; ?>" maxlength="12" >
											</div>
											<div>
												<label for="dob">Birthdate : </label>
												<input type="date" class="form-control" name="dob" value="<?php echo $dob; ?>">
											</div>
											</br>
											<input type="submit" class="btn btn-info" name="update" value="Update">
											<input type="reset" class="btn btn-info" value="Reset">
											</div>
											
											<div class="col-md-6">
											<div>
												<label for="email">Email: </label>
												<input type="email" class="form-control" name="email" value="<?php echo $email; ?>">
											</div>
											<div>
												<label for="phone">Phone:</label>
												<div class="input-group">
													<input type="text" class="form-control" onkeypress="return isNumber(event)" value="<?php if($countph>0){echo $phone[0];} ?>" name="phone" maxlength="3" style="z-index:0;">
													<span class="input-group-addon">-</span>
													<input type="text" class="form-control" onkeypress="return isNumber(event)" value="<?php if($countph>1){echo $phone[1];} ?>" name="phone1" maxlength="7" style="z-index:0;">
												</div>
											</div>
											<div>
												<label for="gender">Gender :</label>
												<div>
												<label class="radio-inline">
													<input type="radio" name="gender" value="Male" <?php if($gender=="Male"){ ?>checked="checked" <?php } ?>>Male
												</label>
												<label class="radio-inline">
													<input type="radio" name="gender" value="Female" <?php if($gender=="Female"){ ?>checked="checked" <?php } ?>>Female
												</label>
												</div>
											</div>
											<div>
												<label for="add1">Address1:</label>
												<input type="text" class="form-control" name="address1" id="address1" 
												value="<?php 
													//echo $addr[0];for($i=1;$i<$count;$i++){echo ", ".$addr[$i];}
													echo $addr[0];
												?>">
											</div>
											<div>
												<label for="add1">Address2:</label>
												<input type="text" class="form-control" name="address2" id="address2" 
												value="<?php 
													//echo $addr[0];for($i=1;$i<$count;$i++){echo ", ".$addr[$i];}
													if($count>1){
														echo $addr[1];
													}
												?>">
											</div>
											</div>
										</form>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
	include 'interface/footer.php';
	}
?>