<?php
	include('../assets/config/config.php');
	error_reporting(0);
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
	<script>
		/*$(document).ready(function () {
			$('#datatable').dataTable({
				"language": {
					"decimal": ",",
					"thousands": ".",
					"lengthMenu": "Show _MENU_ supplier per page",
					"zeroRecords": "Nothing found",
					"info": "",
					"infoEmpty": "No records available",
					"infoFiltered": "(filtered from _MAX_ total records)"
				},
				"ordering": false,
				"bFilter":false,
				"paging":   false,
				//"order": [[ 0, "asc" ]],
				"bLengthChange":false,
				//"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollCollapse": true,
				"autoWidth": false,
				"sScrollX": "100%",
				"sScrollX": "visible: false"
			});							
		});
		jQuery('#dataTable').wrap('<div style="overflow:auto;" />');*/
	</script>

	
	<div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
					<h1 class="page-head-line">Delivery Order</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-md-12">	
                    <div class="row">
					<form method="post" action="#">
					<div class="panel panel-default">
						<div class="panel-heading">
							Delivery Order Form
						</div>
						<?php
							
							if(isset($_POST['save'])){
								$query4=mysql_query("SELECT * FROM customer_purchase_order_details WHERE POID='".$_GET['poid']."'&&item_name!=''");
								$count=mysql_num_rows($query4);
								$doid=$_POST['doid'];
								$po_date=$_POST['po_date'];
								$date=$_POST['date'];
								$poid1=$_POST['poid'];
								//$supplier_id=$_POST['id'];
								for($i=0;$i<$count;$i++){
									$item_name1=$_POST['item_name'][$i];
									$quantity=$_POST['quantity'][$i];
									$sent_quantity=$_POST['sent_quantity'][$i];
									$total_quantity=$_POST['quantity'][$i]-$_POST['sent_quantity'][$i];
									//if($sent_quantity!=""){
										$update=mysql_query("UPDATE stock SET quantity=quantity-".$sent_quantity." 
										WHERE name='".$item_name1."'");
										$update=mysql_query("UPDATE customer_purchase_order_details SET quantity=quantity-".$sent_quantity." 
										WHERE item_name='".$item_name1."' AND POID='".$poid1."'");
										$update=mysql_query("INSERT INTO customer_delivery_order_details
										VALUES ('','$poid1','$doid','$item_name1','$total_quantity','$sent_quantity')");
									//}
								}
								if(!$update){
									echo "<script type=\"text/javascript\">".
										"alert('Invalid value!');".
										"</script>";
								}else{
									$insert=mysql_query("INSERT INTO customer_delivery_order
									VALUES ('','$doid','$po_date','$date','$poid1','".$_SESSION['user_id']."')");
									//header("location:invoice.php");
									echo"<script type=\"text/javascript\">".
										"window.location='customer_dodetails.php?action=success'".
										"</script>";
								}
							}
							//echo '<pre>'; print_r($_POST); echo '</pre>';
							date_default_timezone_set("Asia/Kuala_Lumpur"); 
						?>
						<?php
							if(isset($_GET['poid'])){
								$ID=$_GET['poid'];
								$query3=mysql_query("SELECT * FROM customer_purchase_order INNER JOIN customer ON customer_purchase_order.customer_id=customer.ID WHERE POID=$ID")or die(mysql_error());
								$countq2=mysql_num_rows($query3);
								if($countq2=1){
								while($row = mysql_fetch_assoc($query3)){
									$poid=$row['POID'];
									$date=$row['date'];
									$customer_fname=$row['firstName'];
									$customer_lname=$row['lastName'];
									$status=$row['status'];
									$id=$row['customer_id'];
								}
								}
							}
							/*---------------------------auto DO number-------------------------------------*/
							$query4=mysql_query("SELECT * FROM customer_delivery_order");
							$countall=mysql_num_rows($query4);
							$doid=$countall+1;
							$doid_0fill = str_pad((string)$doid, 5, "0", STR_PAD_LEFT); //auto fill in zero
						?>

						<div class="panel-body">
							<div class="table-responsive">
								<table id="datatable" class="table table-hover cell-border table-bordered" style="border-bottom:1px solid #ddd;">
									<tbody>
										<tr>
											<td width="50%">Delivery ID</td>
											<td>CD<?php echo $doid_0fill; ?><input type="hidden" value="<?php echo $doid; ?>" name="doid"></td>
										</tr>
										<tr>
											<td width="50%">Order ID</td>
											<td>CP<?php echo $poid; ?><input type="hidden" value="<?php echo $poid; ?>" name="poid"></td>
										</tr>
										<tr>
											<td width="50%">Delivery Date</td>
											<td><?php echo date("d-m-Y"); ?><input type="hidden" value="<?php echo date("Y-m-d H:i:s"); ?>" name="date"></td>
										</tr>
										<tr>
											<td width="50%">Customer Name</td>
											<td><?php echo $customer_fname." ".$customer_lname; ?></td>
										</tr>
										<input type="hidden" value="<?php echo $date; ?>" name="po_date">
									</tbody>
								</table>
							</div>
							<div class="table-responsive">
								<table id="datatable" class="table table-striped table-hover" style="border-bottom:1px solid #ddd;">
									<thead>
										<tr>
											<th>No</th>
											<th>Items Name</th>
											<th>Quantity</th>
											<th>Sent Quantity</th>
										</tr>
									</thead>
									<tbody>
									<?php
										$no=1; 
										$query4=mysql_query("SELECT * FROM customer_purchase_order_details WHERE POID='".$_GET['poid']."'&&item_name!=''");
										while($row=mysql_fetch_assoc($query4)){
									?>
									<tr>
										<td width="4%"><?php echo $no++; ?></td>
										<td><input type="hidden" name="item_name[]" value="<?php echo $row['item_name']; ?>" class="form-control"><?php echo $row['item_name']; ?></td>
										<td><input type="hidden" name="quantity[]" value="<?php echo $row['quantity']; ?>" class="form-control"><?php echo $row['quantity']; ?></td>
										<td><input type="number" class="form-control" name="sent_quantity[]" value="0" min="0" max="<?php echo $row['quantity']; ?>"></td>
									</tr>
									<?php
										}
									?>
									</tbody>
								</table>	
								</br>
								<input type="submit" class="btn btn-info" name="save" value="Save" ><!--onclick="return confirm('Are you sure?');"-->
									<input class="btn btn-info" type="button" onclick="history.back();" value="Back">
							</div>
						</div>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
<?php
	include 'interface/footer.php';
	}
?>
