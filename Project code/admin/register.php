<?php
	include('../assets/config/config.php');
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
	include 'interface/head.php';
	?>
	<SCRIPT language=Javascript>
      <!--
		function isNumberKey(evt)
		{
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
		}
		
		function isNumber(evt, element) {

			var charCode = (evt.which) ? evt.which : event.keyCode

			if (
				(charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
				(charCode < 48 || charCode > 57)){
				return false;
				}else{
				return true;
				}
		}
      //-->
	</SCRIPT>
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">User Register</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="panel panel-default">
						<div class="panel-heading">
							New User
						</div>
						<div class="panel-body">
						<ul class="tabs">
						<li><a href="#admin">Admin</a></li>
						<li><a href="#staff">Staff</a></li>
						<li><a href="#customer">Customer</a></li>
						<li><a href="#supplier">Supplier</a></li>
						</ul>
						<!--------------------------------Admin Register--------------------------------------------->
						<div id="admin" class="tab-content">
						<form role="form1" action="php_function/reg.php" method="POST">
						<div class="col-md-6">
							<div>
								<label for="name">Name:</label>
								<div class="input-group">
									<span class="input-group-addon">First Name</span>
									<input type="text" class="form-control" name="fname" required style="z-index:0;">
									<span class="input-group-addon">Last Name</span>
									<input type="text" class="form-control" name="lname" required style="z-index:0;">
								</div>
							</div>
							<div>
								<label for="ic">I/C No: </label>
								<div class="input-group">
								<input type="text" class="form-control" name="ic" maxlength="6" placeholder="950101" onkeypress="return isNumber(event)" required style="z-index:0;">
									<span class="input-group-addon">-</span>
								<input type="text" class="form-control" name="ic1" maxlength="2" placeholder="01" onkeypress="return isNumber(event)" required style="z-index:0;">
									<span class="input-group-addon">-</span>
								<input type="text" class="form-control" name="ic2" maxlength="4" placeholder="6666" onkeypress="return isNumber(event)" required style="z-index:0;">
								</div>
							</div>
							<div>
								<label for="fname">Username:</label>
								<input type="text" class="form-control" name="username" required>
							</div>
							<div>
								<label for="fname">Password:</label>
								<input type="password" class="form-control" name="password" maxlength="12" required>
							</div>
							<div>
								<label for="gender">Gender :</label>
								<div>
								<label class="radio-inline">
									<input type="radio" name="gender" value="Male" checked="">Male
								</label>
								<label class="radio-inline">
									<input type="radio" name="gender" value="Female">Female
								</label>
								</div>
							</div>
							</br>
							<input type="submit" class="btn btn-info" name="admin_register" value="Register">
							<input type="reset" class="btn btn-info" value="Reset">
						</div>
						<div class="col-md-6">
							<div>
								<label for="dob">Birthdate : </label>
								<input type="date" class="form-control" name="dob" required>
							</div>
							<div>
								<label for="email">Email: </label>
								<input type="email" class="form-control" name="email" required>
							</div>
							<div>
								<label for="phone">Phone:</label>
								<div class="input-group">
									<input type="text" class="form-control" onkeypress="return isNumber(event)" name="phone" maxlength="3" required style="z-index:0;">
									<span class="input-group-addon">-</span>
									<input type="text" class="form-control" onkeypress="return isNumber(event)" name="phone1" maxlength="7" required style="z-index:0;">
								</div>
							</div>
							<div>
								<label for="add1">Address1:</label>
								<input type="text" class="form-control" name="address1">
							</div>
							<div>
								<label for="add1">Address2:</label>
								<input type="text" class="form-control" name="address2">
							</div>
						</form>
						</div>
						</div>
						<!--------------------------------Staff Register--------------------------------------------->
						<div id="staff" class="tab-content">
						<form role="form2" action="php_function/reg.php" method="POST">
						<div class="col-md-6">
							<div>
								<label for="name">Name:</label>
								<div class="input-group">
									<span class="input-group-addon">First Name</span>
									<input type="text" class="form-control" name="fname" required style="z-index:0;">
									<span class="input-group-addon">Last Name</span>
									<input type="text" class="form-control" name="lname" required style="z-index:0;">
								</div>
							</div>
							<div>
								<label for="ic">I/C No: </label>
								<div class="input-group">
								<input type="text" class="form-control" name="ic" maxlength="6" placeholder="950101" onkeypress="return isNumber(event)" required style="z-index:0;">
									<span class="input-group-addon">-</span>
								<input type="text" class="form-control" name="ic1" maxlength="2" placeholder="01" onkeypress="return isNumber(event)" required style="z-index:0;">
									<span class="input-group-addon">-</span>
								<input type="text" class="form-control" name="ic2" maxlength="4" placeholder="6666" onkeypress="return isNumber(event)" required style="z-index:0;">
								</div>
							</div>
							<div>
								<label for="fname">Username:</label>
								<input type="text" class="form-control" name="username" required>
							</div>
							<div>
								<label for="fname">Password:</label>
								<input type="password" class="form-control" name="password" maxlength="12" required>
							</div>
							<div>
								<label for="dob">Birthdate : </label>
								<input type="date" class="form-control" name="dob" required>
							</div>
							<div>
								<label for="email">Email: </label>
								<input type="email" class="form-control" name="email" required>
							</div>
							</br>
							<input type="submit" class="btn btn-info" name="staff_register" value="Register">
							<input type="reset" class="btn btn-info" value="Reset">
						</div>
						<div class="col-md-6">
							<div>
								<label for="phone">Phone:</label>
								<div class="input-group">
									<input type="text" class="form-control" onkeypress="return isNumber(event)" name="phone" maxlength="3" required style="z-index:0;">
									<span class="input-group-addon">-</span>
									<input type="text" class="form-control" onkeypress="return isNumber(event)" name="phone1" maxlength="7" required style="z-index:0;">
								</div>
							</div>
							<div>
								<label for="gender">Gender :</label>
								<div>
								<label class="radio-inline">
									<input type="radio" name="gender" value="Male" checked="">Male
								</label>
								<label class="radio-inline">
									<input type="radio" name="gender" value="Female">Female
								</label>
								</div>
							</div>
							<div>
								<label for="add1">Address1:</label>
								<input type="text" class="form-control" name="address1" id="address1" required>
							</div>
							<div>
								<label for="add1">Address2:</label>
								<input type="text" class="form-control" name="address2" id="address2" required>
							</div>
							<div>
								<label for="hire_date">Hire date : </label>
								<input type="date" class="form-control" name="hire_date" required>
							</div>
							<div>
								<label for="salary">Basic salary : </label>
								<input type="text" class="form-control" name="basic_salary" onkeypress="return isNumber(event)" required>
							</div>
						</form>
						</div>
						</div>
						<!--------------------------------Customer Register--------------------------------------------->
						<div id="customer" class="tab-content">
						<form role="form3" action="php_function/reg.php" method="POST">
						<div class="col-md-6">
							<div>
								<label for="name">Name:</label>
								<div class="input-group">
									<span class="input-group-addon">First Name</span>
									<input type="text" class="form-control" name="fname" required style="z-index:0;">
									<span class="input-group-addon">Last Name</span>
									<input type="text" class="form-control" name="lname" required style="z-index:0;">
								</div>
								<!--<label for="ic">NRIC: </label>
								&nbsp;
								<input type="text" style="width:8%;" class="form-control" name="nric" maxlength="6" onkeypress="return isNumberKey(event)">
								-
								<input type="text" style="width:5%;" class="form-control" name="nric1" maxlength="2" onkeypress="return isNumberKey(event)">
								-
								<input type="text" style="width:8%;" class="form-control" name="nric2" maxlength="4" onkeypress="return isNumberKey(event)">
								-->
							</div>
							<div>
								<label for="fname">Username:</label>
								<input type="text" class="form-control" name="username" required>
							</div>
							<div>
								<label for="fname">Password:</label>
								<input type="password" class="form-control" name="password" maxlength="12" required>
							</div>
							<!--<div>
								<label for="dob">DOB : </label>
								<input type="date" class="form-control" name="dob" maxlength="10" placeholder="dd/mm/yyyy" maxlength="8" >
							</div>-->
							<div>
								<label for="email">Email: </label>
								<input type="email" class="form-control" name="email" required>
							</div>
							<div>
								<label for="country">Country:</label>
								<select class="form-control" name="select_country">
									<option selected="selected" value="sc">--Select Country--</option>
								<?php
									$q = mysql_query("SELECT * FROM country ORDER BY nicename ASC");
									while($row2=mysql_fetch_assoc($q)){
								?>
									<option value="<?php echo $row2['iso']; ?>"><?php echo $row2['nicename']." &rlm;"."(&lrm;+".$row2['phonecode'].")"; ?></option>
								<?php
									}
								?>
								</select>
							</div>
							<div>
								<label for="gender">Gender :</label>
								<div>
								<label class="radio-inline">
									<input type="radio" name="gender" value="Male" checked="">Male
								</label>
								<label class="radio-inline">
									<input type="radio" name="gender" value="Female">Female
								</label>
								</div>
							</div>
							</br>
							<input type="submit" class="btn btn-info" name="customer_register" value="Register">
							<input type="reset" class="btn btn-info" value="Reset">
						</div>
						<div class="col-md-6">
							<div>
								<label for="phone">Phone:</label>
								<div class="input-group">
									<input type="text" class="form-control" onkeypress="return isNumberKey(event)" name="phone" maxlength="3" required style="z-index:0;">
									<span class="input-group-addon">-</span>
									<input type="text" class="form-control" onkeypress="return isNumberKey(event)" name="phone1" maxlength="7" required style="z-index:0;">
								</div>
							</div>
							<div>
								<label for="add1">Address:</label>
								<input class="form-control" type="text" name="address1" id="address1" required>
							</div>
							<div>
								<label for="add1">Address2:</label>
								<input class="form-control" type="text" name="address2" id="address2" style="margin:5px 0;">
								<!--<textarea class="form-control" name="address" id="address" required></textarea>-->
							</div>
							<div>
								<label for="cname">Company Name:</label>
								<input type="text" class="form-control" name="cname" >
							</div>
							<div>
								<label for="cphone">Company Phone:</label>
								<div class="input-group">
									<input type="text" class="form-control" onkeypress="return isNumberKey(event)" name="cphone" maxlength="2" required style="z-index:0;">
									<span class="input-group-addon">-</span>
									<input type="text" class="form-control" onkeypress="return isNumberKey(event)" name="cphone1" maxlength="7" required style="z-index:0;">
								</div>
								<!--<input type="text" class="form-control" placeholder="E.g. 00-0000000" name="cphone" maxlength="10" onkeypress="return isNumber(event,this)">-->
							</div>
						</form>
						</div>
						</div>
						<!--------------------------------Supplier Register--------------------------------------------->
						<div id="supplier" class="tab-content">
						<form role="form4" action="#" method="POST">
						<div class="col-md-6">
							<div>
								<label for="cname">Company Name:</label>
								<input type="text" class="form-control" name="company_name" required>
							</div>
							<div>
								<label for="phone">Country:</label>
								<select class="form-control" name="select_country">
									<option selected="selected" value="sc">--Select Country--</option>
								<?php
									$q = mysql_query("SELECT * FROM country ORDER BY nicename ASC");
									while($row2=mysql_fetch_assoc($q)){
								?>
									<option value="<?php echo $row2['iso']; ?>"><?php echo $row2['nicename']." &rlm;"."(&lrm;+".$row2['phonecode'].")"; ?></option>
								<?php
									}
								?>
								</select>
							</div>
							<div>
								<label for="cphone">Company Phone:</label>
								<div class="input-group">
									<input type="text" class="form-control" onkeypress="return isNumberKey(event)" name="company_phone" maxlength="2" required style="z-index:0;">
									<span class="input-group-addon">-</span>
									<input type="text" class="form-control" onkeypress="return isNumberKey(event)" name="company_phone1" maxlength="7" required style="z-index:0;">
								</div>
							</div>
							<div>
								<label for="cname">Person In Charge:</label>
								<input type="text" class="form-control" name="pic" >
							</div>
							</br>
							<input type="submit" class="btn btn-info" name="supplier_register" value="Register">
							<input type="reset" class="btn btn-info" value="Reset">
						</div>
						<div class="col-md-6">
							<div>
								<label for="phone">Phone:</label>
								<div class="input-group">
									<input type="text" class="form-control" onkeypress="return isNumberKey(event)" name="supplier_phone" maxlength="3" required style="z-index:0;">
									<span class="input-group-addon">-</span>
									<input type="text" class="form-control" onkeypress="return isNumberKey(event)" name="supplier_phone1" maxlength="7" required style="z-index:0;">
								</div>
							</div>
							<div>
								<label for="cname">Email:</label>
								<input type="text" class="form-control" name="supplier_email" required>
							</div>
							<div>
								<label for="add1">Address:</label>
								<textarea class="form-control" name="address" id="address" required></textarea>
							</div>
						</form>
						</div>
						</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
				
<?php
	include 'interface/footer.php';
	}
?>