<?php
	include('../assets/config/config.php');
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
		<script>
		$(document).ready(function () {
			$('#datatable').dataTable({
				"language": {
					"decimal": ",",
					"thousands": ".",
					"lengthMenu": "Show _MENU_ stock adjustment details per page",
					"zeroRecords": "Nothing found",
					"info": "Showing _START_ to _END_ of _TOTAL_ stock adjustment details",
					"infoEmpty": "No records available",
					"infoFiltered": "(filtered from _MAX_ total records)"
				},
				"order": [[ 0, "asc" ]],
				/*"bLengthChange":false,
				"pageLength": 10
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollCollapse": true,
				"autoWidth": false,
				"sScrollX": "100%",
				"sScrollX": "visible: false"*/
			});							
		});
		jQuery('#dataTable').wrap('<div style="overflow:auto;" />');
	</script>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
						<h1 class="page-head-line">Stock Adjustment</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">	
						<?php
							$action = isset($_GET['action']) ? $_GET['action'] : "";
							if($action=="success"){
								echo "<div class='alert alert-info'>";
									echo "Create successfully!";
								echo "</div>";
							}
						?>
                        <div class="row">
						<form id="form1" method="post" action="#">
							<div class="panel panel-default">
								<div class="panel-heading">
									Detail
								</div>
								<div class="panel-body">
									<a href="adjuststock.php" class="btn btn-default btn-outline" style="float:right;margin:-7 0 5 0;">Add Adjustment</a>
									<div class="table-responsive">
										<table id="datatable" class="display cell-border table-bordered table-striped" style="border-bottom:1px solid #ddd;">
											<thead>
												<tr>
													<th>Adjustment No</th>
													<th>Date</th>
													<th>Reason</th>
													<th>Total Adjustment Quantity</th>
													<th>Total Adjustment Value</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
											<?php
												$query = mysql_query("SELECT * FROM stock_adjustment")or die(mysql_error());
												while($row = mysql_fetch_assoc($query)){
											?>
												<tr>
													<td><?php echo "SA".$row['ID']; ?></td>
													<td><?php echo $row['date']; ?></td>
													<td><?php echo ucfirst($row['reason']); ?></td>
													<td style="text-align:right;"><?php echo $row['quantity']; ?></td>
													<td style="text-align:right;">RM <?php echo number_format($row['total'],2); ?></td>
													<td><a href="view_as.php?id=<?php echo $row['ID'];?>" class="btn btn-primary btn-outline"><i class="fa fa-eye" aria-hidden="true">&nbsp;</i>View Detail</a></td>
												</tr>
											<?php
												}
											?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
	include 'interface/footer.php';
	}
?>