<?php
	include('../assets/config/config.php');
	
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
		
	if(isset($_POST['update'])){
		//$item=$_POST['item'];
		$reason=$_POST['reason'];
		$quantity=$_POST['quantity'];
		$value=$_POST['value'];
		$total=$_POST['total'];
		
		$query=mysql_query("UPDATE stock_adjustment SET reason='$reason',lastModify=now(),quantity='$quantity',value='$value',total='$total'");
		$query=mysql_query("UPDATE stock SET quantity=quantity-'$quantity'");
	}
	
	$query1=mysql_query("SELECT * FROM stock_adjustment WHERE ID=".$_GET['id']."");
	while($row=mysql_fetch_assoc($query1)){
		$item_name=$row['item'];
		$reason=$row['reason'];
		$date=$row['date'];
		$lastModify=$row['lastModify'];
		$quantity=$row['quantity'];
		$value=$row['value'];
	}
	
	$query2=mysql_query("SELECT * FROM stock WHERE name='".$item_name."'");
	while($row1=mysql_fetch_assoc($query2)){
		$onhandqty=$row1['quantity'];
		$onhandvalue=$row1['quantity']*$row1['companyPrice'];
	}
?>
        <div id="page-wrapper"">
            <div id="page-inner" style="min-height:550;">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Stock Adjustments</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
						<div class="row">
							<div class="panel panel-default">
								<div class="panel-heading">
									SA<?php echo $_GET['id'];?>
								</div>
								<div class="panel-body">
									<form role="form" action="#" method="POST" accept-charset="utf-8">
									<script>
										$(".item").ready(function(){
											selection();
										});
										function caltotal(value){
											var quantity=$("#quantity").val();		
											var price=Number($("#value").val().replace(/[^0-9\.]+/g,""));
											var total = parseFloat(quantity)*parseFloat(price);
											console.log(quantity);
											console.log(price);
											if(quantity>0){
											$("#total").val(parseFloat(total).toFixed(2));
											}else{
											$("#total").val(parseFloat(0).toFixed(2));
											}
										}
										function changeradioother(){
										var other= document.getElementById("other");
										other.value=document.getElementById("inputother").value;
										}
									</script>
									<div class="col-md-12">
									<div class="col-md-6">
										<div>
											<label>Item:</label>
											&nbsp;&nbsp;
											<input type="text" class="form-control" value="<?php echo $item_name; ?>" readonly>
										</div>
										<div>										  
											  <label for="Reason">Reason :</label>
												<div>
												<label class="radio-inline">
													<input type="radio" name="reason" value="sample"<?php if($reason=="sample"){?>checked<?php } ?>>Sample
												</label>
												<label class="radio-inline">
													 <input type="radio" name="reason" value="stolen"<?php if($reason=="stolen"){?>checked<?php } ?>>Stolen
												</label>
												<label class="radio-inline">
													 <input type="radio" name="reason" value="missing"<?php if($reason=="missing"){?>checked<?php } ?>>Missing
												</label>
												<label class="radio-inline">
													 <input type="radio" name="reason" id="other" value="other"<?php if(($reason!="missing")&&($reason!="sample")&&($reason!="stolen")){?>checked<?php } ?>>Other 
													<input  id="inputother" type="text" onchange="changeradioother()" value="<?php if(($reason!="missing")&&($reason!="sample")&&($reason!="stolen")){echo $reason;} ?>"/>
												</label>
												</div>
										</div>
										<div>
											<label>Date</label>
											<input type="text" class="form-control" value="<?php echo $date; ?>" disabled>
										</div>
										<div>
											<label>On Hand Quantity</label>
											<input type="text" class="form-control" value="<?php echo $onhandqty; ?>" disabled>
										</div>
										<div>
											<label>Last Modified On</label>
											<input type="text" class="form-control" value="<?php echo $lastModify; ?>" disabled>
										</div>
									</div>
									<div class="col-md-6">
										<div>
											<label>Quantity</label>
											<input type="number" class="form-control" name="quantity" id="quantity" 
											onchange="caltotal(this);" value="<?php echo $quantity; ?>" min="1" max="<?php echo $onhandqty;?>">
										</div>
										<div>
											<label>Value</label>
											<input type="text" class="form-control" name="value" id="value"readonly="readonly" value="<?php echo number_format($value,2); ?>">
										</div>
										<div>
											<label>Total Value</label>
											<input type="text" class="form-control" name="total" id="total"readonly="readonly" value="<?php echo number_format($quantity*$value,2); ?>">
										</div>
										<div>
											<label>On Hand Value</label>
											<input type="text" class="form-control" value="<?php echo number_format($onhandvalue,2) ?>" disabled>
										</div>
									</div>
									</div>
								<input type="submit" name="update" value="Update" class="btn btn-info" style="float:right;margin:15 30 0 0;">
									</form>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
<?php
	include 'interface/footer.php';
	}
?>