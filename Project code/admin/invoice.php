<?php
	include('../assets/config/config.php');
	error_reporting(0);
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
	<div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
					<h1 class="page-head-line">Invoice</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-md-12">	
                    <div class="row">
					<form method="post" action="invoice.php">
					<div class="panel panel-default">
						<div class="panel-body">
						<?php
							if(isset($_GET['id'])){
								$ID=$_GET['id'];
								$query2=mysql_query("SELECT * FROM purchase_order INNER JOIN supplier ON purchase_order.supplier_id=supplier.ID WHERE POID='$ID'")or die(mysql_error());
								while($row = mysql_fetch_assoc($query2)){
									$id=$row['ID'];
									$poid=$row['POID'];
									$date=$row['date'];
									$name=$row['person_in_charge'];
									$compn=$row['company_name'];
									$phone=$row['phone'];
									$cphone=$row['company_phone'];
									$address=$row['address'];
									$email=$row['email'];
								}
							}
							date_default_timezone_set("Asia/Kuala_Lumpur"); 
						?>
							<div class="printsize" style="float:left;position:relative;">
							<label><u>Supplier Detail</u></label>
							<table class="adjusttd">
								<tr>
									<td>Supplier ID:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $id; ?>" name="id"><?php echo "S".$id; ?></td>
								</tr>
								<tr>
									<td>Name:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $name; ?>" name="name"><?php echo $name; ?></td>
								</tr>
								<tr>
									<td>Phone Number:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $phone; ?>" name="phone"><?php echo $phone; ?></td>
								</tr>
								<tr>
									<td>Company:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $compn; ?>" name="compn"><?php echo $compn; ?></td>
								</tr>
								<tr>
									<td>Company P/H:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $cphone; ?>" name="cphone"><?php echo $cphone; ?></td>
								</tr>
								<tr>
									<td>Email:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $email; ?>" name="email"><?php echo $email; ?></td>
								</tr>
								<tr>
									<td>Address:</td>
									<td style="width:5px;"><input type="hidden" value="<?php echo $address; ?>" name="address"><?php echo $address; ?></td>
								</tr>
							</table>
							</div>
							<div class="printsize" style="float:left;margin:0 0 0 20%;">
							<label><u>Invoice Detail</u></label>
							<table class="adjusttd">
								<tr>
									<td>Purchase Order No:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $poid; ?>" name="poid"><?php echo "P".$poid; ?></td>
								</tr>
								<tr>
									<td>Date:&nbsp;</td>
									<td><input type="hidden" value="<?php echo date("Y/m/d"); ?>" name="date"><?php echo date("d/m/Y"); ?></td>
								</tr>
							</table>
							</div>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table id="datatable" class="table table-striped table-hover" style="border-bottom:1px solid #ddd;">
									<thead>
										<tr>
											<th>No</th>
											<th>Items Name</th>
											<th>Quantity</th>
											<th>Unit Price</th>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
									<?php
										$no=1;
										$subtotal=0;											
										$query4=mysql_query("SELECT * FROM purchase_order_details WHERE POID='".$_GET['id']."' AND item_name!=''");
										while($row2=mysql_fetch_assoc($query4)){
									?>
									<tr>
										<td width="4%"><?php echo $no++;?></td>
										<td><?php echo $row2['item_name'];?></td>
										<td><?php echo $row2['quantity'];?></td>
										<td><?php echo "RM".number_format($row2['unit_price'],2);?></td>
										<td><?php echo "RM".number_format($row2['total_price'],2);?></td>
										<?php 
											$subtotal+=$row2['total_price'];
										?>
									</tr>
									<?php 
										}
									?>
									
									</tbody>
									<tr>
										<td colspan="4" style="text-align:right;">Subtotal:</td>
										<td><?php echo "RM".number_format($subtotal,2) ?></td>
									</tr>
								</table>
								<a class="print btn btn-default" onclick="print(document)"><span class="glyphicon glyphicon-print"></span> Print</a>
							</div>
						</div>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
<?php
	include 'interface/footer.php';
	}
?>
