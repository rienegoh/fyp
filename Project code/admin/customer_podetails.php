<?php
	include('../assets/config/config.php');
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
		<script>
		$(document).ready(function () {
			$('#datatable').dataTable({
				"language": {
					"decimal": ",",
					"thousands": ".",
					"lengthMenu": "Show _MENU_ customer purchase order per page",
					"zeroRecords": "Nothing found",
					"info": "Showing _START_ to _END_ of _TOTAL_ customer purchase order",
					"infoEmpty": "No records available",
					"infoFiltered": "(filtered from _MAX_ total records)"
				},
				"order": [[ 0, "asc" ]],
				/*"bLengthChange":false,
				"pageLength": 10
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollCollapse": true,
				"autoWidth": false,
				"sScrollX": "100%",
				"sScrollX": "visible: false"*/
			});							
		});
		jQuery('#dataTable').wrap('<div style="overflow:auto;" />');
	</script>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
						<h1 class="page-head-line">Customer Purchase Order</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">	
                        <div class="row">
						<form id="form1" method="post" action="#">
							<div class="panel panel-default">
								<div class="panel-heading">
									Customer Purchase Order Detail
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table id="datatable" class="display cell-border table-bordered table-striped" style="border-bottom:1px solid #ddd;">
											<thead>
												<tr>
													<th>PO No</th>
													<th>Cusotmer</th>
													<th>Contact No</th>
													<th>Email Address</th>
													<th>Status</th>
													<th width="10%">View PO</th>
													<th width="10%">Create DO</th>
												</tr>
											</thead>
											<tbody>
											<?php
												$query = mysql_query("SELECT * FROM customer_purchase_order INNER JOIN customer ON customer_purchase_order.customer_id=customer.ID")or die(mysql_error());
												while($row = mysql_fetch_assoc($query)){
											?>
												<tr>
													<td><?php echo "CP".$row['POID']; ?></td>
													<td><?php echo $row['firstName']." ".$row['lastName']; ?></td>
													<td><?php echo $row['phone'];?></td>
													<td><?php echo $row['email'];?></td>
													<td><?php echo $row['status'];?></td>
													<td><a href="view_customer_po.php?poid=<?php echo $row['POID'];?>" class="btn btn-primary btn-outline"><i class="fa fa-eye" aria-hidden="true">&nbsp;</i>View Details</a></td>
													<td><a href="customer_do.php?poid=<?php echo $row['POID'];?>" class="btn btn-primary btn-outline"><i class="glyphicon glyphicon-edit">&nbsp;</i>Create</a></td>
												</tr>
											<?php
												}
											?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
	include 'interface/footer.php';
	}
?>
