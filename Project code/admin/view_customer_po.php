<?php
	include('../assets/config/config.php');
	error_reporting(0);
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
	<script>
		/*$(document).ready(function () {
			$('#datatable').dataTable({
				"language": {
					"decimal": ",",
					"thousands": ".",
					"lengthMenu": "Show _MENU_ supplier per page",
					"zeroRecords": "Nothing found",
					"info": "",
					"infoEmpty": "No records available",
					"infoFiltered": "(filtered from _MAX_ total records)"
				},
				"ordering": false,
				"bFilter":false,
				"paging":   false,
				//"order": [[ 0, "asc" ]],
				"bLengthChange":false,
				//"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollCollapse": true,
				"autoWidth": false,
				"sScrollX": "100%",
				"sScrollX": "visible: false"
			});							
		});
		jQuery('#dataTable').wrap('<div style="overflow:auto;" />');*/
	</script>

	<?php 
		if(isset($_POST['completed'])){
			$complete=$_POST['completed_value'];
			$id=$_GET['poid'];
			
			$query3=mysql_query("UPDATE customer_purchase_order SET status='$complete' WHERE POID='$id'" );
		}
		if(isset($_POST['pending'])){
			$pending=$_POST['pending_value'];
			$id=$_GET['poid'];
			
			$query3=mysql_query("UPDATE customer_purchase_order SET status='$pending' WHERE POID='$id'" );
		}
	?>
	<div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
					<h1 class="page-head-line">Customer Order</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-md-12">	
                    <div class="row">
					<form method="post" action="#">
					<div class="panel panel-default">
						<div class="panel-heading">
							Customer Order Details
						</div>
						<div class="panel-body">
						<?php
							if(isset($_GET['poid'])){
								$ID=$_GET['poid'];
								$query2=mysql_query("SELECT * FROM customer_purchase_order INNER JOIN customer ON customer_purchase_order.customer_id=customer.ID WHERE POID=$ID")or die(mysql_error());
								$countq2=mysql_num_rows($query2);
								if($countq2=1){
								while($row = mysql_fetch_assoc($query2)){
									$poid=$row['POID'];
									$date=$row['date'];
									$customer_fname=$row['firstName'];
									$customer_lname=$row['lastName'];
									$status=$row['status'];
									$id=$row['customer_id'];
								}
								$query5=mysql_query("SELECT * FROM customer_purchase_order_details WHERE POID='".$_GET['poid']."' AND item_name!=''");
								while($row1 = mysql_fetch_assoc($query5)){
									$subtotal=$row1['total_price'];
									$total+=$subtotal;
								}

								}else{
									$id='';
									$poid='';
									$name='';
									$compn='';
									$phone='';
									$cphone='';
									$address='';
									$email='';
									$date='';
								}
							}	
						?>
							<div class="table-responsive">
								<table id="datatable" class="table table-hover cell-border table-bordered" style="border-bottom:1px solid #ddd;">
									<tbody>
										<tr>
											<td width="50%">Order ID</td>
											<td>CP<?php echo $poid; ?></td>
										</tr>
										<tr>
											<td width="50%">Order Date</td>
											<td><?php echo $date; ?></td>
										</tr>
										<tr>
											<td width="50%">Customer Name</td>
											<td><?php echo $customer_fname." ".$customer_lname; ?></td>
										</tr>
										<tr>
											<td width="50%">Total Cost</td>
											<td>RM<?php echo number_format($total,2); ?></td>
										</tr>
										<tr>
											<td width="50%">Status</td>
											<td>
											<?php
												$query6=mysql_query("SELECT * FROM customer_purchase_order_details WHERE POID='".$_GET['poid']."' AND quantity='0'");
												$countmysql=count($query6);
											?>
												<div class="btn-group" role="group" aria-label="Basic example">
													<button type="submit" class="btn btn-default" name="pending" <?php if($status=="Pending"){ ?>disabled<?php } ?>>Pending</button>
													<button type="submit" class="btn btn-default" name="completed" <?php if(($status=="Completed")){ ?>disabled<?php } ?>>Completed</button>
												</div>
													<input type="hidden" name="pending_value" value="Pending">
													<input type="hidden" name="completed_value" value="Completed">
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table id="datatable" class="table table-striped table-hover" style="border-bottom:1px solid #ddd;">
									<thead>
										<tr>
											<th>No</th>
											<th>Item</th>
											<th>Quantity</th>
											<th>Unit Price (RM)</th>
											<th>Sub Total (RM)</th>
										</tr>
									</thead>
									<tbody>
									<?php
										$no=1;
										$subtotal=0;
										$query4=mysql_query("SELECT * FROM customer_purchase_order_details WHERE POID='".$_GET['poid']."' AND item_name!=''");
										while($row2=mysql_fetch_assoc($query4)){
											//$id=$row2['ID'];
									?>
									<tr>
										<td><?php echo $no++;?></td>
										<td><?php echo $row2['item_name'];?></td>
										<td><?php if($row2['quantity']!='0'){echo $row2['quantity'];}else{echo 'Complete';} ?></td>
										<td class="price"><?php echo number_format($row2['unit_price'],2);?></td>
										<td><?php echo number_format($row2['total_price'],2);?></td>
										<?php 
											$subtotal+=$row2['total_price'];
										?>
									</tr>
									<?php 
										}
									?>
									</tbody>
									<tr>
										<td colspan="4" style="text-align:right;">Total:</td>
										<td><?php echo number_format($subtotal,2) ?></td>
									</tr>
								</table>
								</br>
								<a class="print btn btn-default" onclick="print(document)"><span class="glyphicon glyphicon-print"></span> Print</a>
								<!--<a href="invoice.php?id=<?php echo $_GET['poid'];?>" name="generate" class="btn btn-success">Generate Invoice</a>-->
									<input class="btn btn-default" type="button" onclick="history.back();" value="Back">
							</div>
						</div>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
<?php
	include 'interface/footer.php';
	}
?>
