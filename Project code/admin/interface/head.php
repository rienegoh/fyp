<html>
<head runat="server">
	<meta charset="utf-8" />
    <title>Zhuan You Hardware Enterprise</title>
    <script src="../assets/js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../assets/css/jquery.dataTables.min.css" />
	<link href="../assets/css/bootstrap.css" rel="stylesheet" />
	<!--<link href="../assets/css/bootstrap-fileupload.min.css" rel="stylesheet" />-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <link href="../assets/css/basic.css" rel="stylesheet" />
    <!--<link href="../assets/css/custom.css" rel="stylesheet" />-->
    <link href="../assets/css/style.css" rel="stylesheet" />
    <!--<script src="../assets/js/custom.js"></script>-->
	<script src="../assets/js/jquery.min.js"></script>
	<!--<script src="../assets/js/bootstrap.min.js"></script>-->
	<script src="../assets/js/jquery.dataTables.min.js"></script>
	<!--<script src="../assets/js/bootstrap-fileupload.js"></script>-->
	<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
	<script type="text/javascript" src="../assets/js/kickstart.js"></script>            
	<!--<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">-->
	<link rel="stylesheet" type="text/css" media="all" href="../assets/css/datepicker/daterangepicker.css" />
	<script type="text/javascript" src="../assets/js/datepicker/moment.js"></script>
	<script type="text/javascript" src="../assets/js/datepicker/daterangepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#checkall').click(function(){
				if($(this).is(":checked")){
					$('.group').prop('checked',true);
				}else{
					$('.group').prop('checked',false);
				}
			});
		});

		function checkall(){
			var length = document.forms[0].length;
			for(var i=0;i<length-1;i++){
				document.forms[0].elements[i].checked = true;
			}
		}
	</script>			

	<script>
		$(document).ready(function(){
			$('.nav_bar').click(function(){
				$('.navigation').toggleClass('visible');
				$('body').toggleClass('opacity');
			});
		});
	</script>	
</head>
<body style="font-family: Arial">
<div id="wrapper">
	<div id="navbar" class="navbar_absolute" style="position:fixed;width:100%;z-index:1;">
	<nav>
		<?php 
			
			if($_SESSION['type']=="Administrator"){
		?>
			<div class="navigation">
			<ul>
				
				<!--<li><a href="adminPanel.php" class="not-active"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>-->
				<li><span class="medianav" style="padding: 10px 15px;">Zhuan You</span></li>
				<li><a href="#"><i class="fa fa-archive" aria-hidden="true"></i> Inventory <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<ul class="dropdown">
						<li><a href="inventory.php">View Inventory</a></li>
						<li><a href="addstock.php">Add Items</a></li>
						<li><a href="stock_adjustment_details.php">Stock Adjustment</a></li>
						<li><a href="category.php">View Category</a></li>
						<li><a href="add_category.php">Add Category</a></li>
					</ul>
				</li>
				<li><a href="#"><i class="fa fa-users" aria-hidden="true"></i> User <i class="fa fa-caret-down"></i></a>
					<ul class="dropdown">
						<li><a href="admin.php">Admin Details</a></li>
						<li><a href="staff.php">Staff Details</a></li>
						<li><a href="viewleave.php">Staff Leave Details</a></li>
						<li><a href="member.php">Customer Details</a></li>
						<li><a href="supplier.php">Supplier Details (Create PO)</a></li>
					</ul>
				</li>
				<li><a href="register.php"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add User</a></li>
				<li><a href="#"><i class="fa fa-list-alt" aria-hidden="true"></i> PO & DO <i class="fa fa-caret-down"></i></a>
					<ul class="dropdown">
						<li><a href="podetails.php">PO Details</a></li>
						<li><a href="dodetails.php">DO Details</a></li>
						<li><a href="customer_podetails.php">Customer PO Details</a></li>
						<li><a href="customer_dodetails.php">Customer DO Details</a></li>
					</ul>
				</li>
				<li><a href="customer_purchase.php"><i class="fa fa-list-alt" aria-hidden="true"></i> Report <i class="fa fa-caret-down"></i></a>
					<ul class="dropdown">
						<li><a href="purchaseReport.php">Purchase Report</a></li>
						<li><a href="salesReport.php">Salaes Report</a></li>
					</ul>
				</li>
				<li><a href="#"><i class="fa fa-list-alt" aria-hidden="true"></i> Transport <i class="fa fa-caret-down"></i></a>
					<ul class="dropdown">
						<li><a href="viewtransport.php">view transport</a></li>
					</ul>
				</li>
				<span class="change right left">
					<?php
						$q = mysql_query("SELECT * FROM admin WHERE username='".$_SESSION['username']."' AND type='".$_SESSION['type']."'");
						//$q2 = mysql_query("SELECT * FROM customer WHERE username='".$_SESSION['username']."' AND type='".$_SESSION['type']."'");
						$countq=mysql_num_rows($q);
						//$countq2=mysql_num_rows($q2);
						//if($countq>1){
							while($row = mysql_fetch_assoc($q)){
								$fname = $row['firstName'];
								$lname = $row['lastName'];
								$id = $row['ID'];
								//$type = $row['type'];
							}
						/*}else if($countq2>1){
							while($row = mysql_fetch_assoc($q2)){
								$name = $row['name'];
								$id = $row['ID'];
								//$type = $row['type'];
							}
						}*/
					?>
					<li><a href="userprofile.php?id=<?php echo $id; ?>"><span class="glyphicon glyphicon-user"></span> <?php echo $fname." ".$lname; ?></a></li>
					<li><a href="logout.php"><span class="glyphicon glyphicon-off"></span> Log Out</a></li>
				</span>
			</ul>
			</div>
			<?php 
			}else if($_SESSION['type']=="Staff"){
			?>
			<div class="navigation">
			<ul>
				
				<!--<li><a href="adminPanel.php" class="not-active"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>-->
				<li><span class="medianav" style="padding: 10px 15px;">Zhuan You</span></li>


				
				<li><a href="#"><i class="fa fa-list-alt" aria-hidden="true"></i> PO & DO <i class="fa fa-caret-down"></i></a>
					<ul class="dropdown">
						<li><a href="customer_podetails.php">Customer PO Details</a></li>
						<li><a href="customer_dodetails.php">Customer DO Details</a></li>
					</ul>
				</li>
				<li><a href="#"><i class="fa fa-list-alt" aria-hidden="true"></i> Customer Purchase</a></li>
				<li><a href="transport.php"><i class="fa fa-list-alt" aria-hidden="true"></i> Transport</a></li>
				<li><a href="#"><i class="fa fa-list-alt" aria-hidden="true"></i> Task</a></li>
				<span class="change right left">
					<?php
						$q = mysql_query("SELECT * FROM staff WHERE username='".$_SESSION['username']."' AND type='".$_SESSION['type']."'");
						//$q2 = mysql_query("SELECT * FROM customer WHERE username='".$_SESSION['username']."' AND type='".$_SESSION['type']."'");
						$countq=mysql_num_rows($q);
						//$countq2=mysql_num_rows($q2);
						//if($countq>1){
							while($row = mysql_fetch_assoc($q)){
								$fname = $row['firstName'];
								$lname = $row['lastName'];
								$id = $row['ID'];
								//$type = $row['type'];
							}
						/*}else if($countq2>1){
							while($row = mysql_fetch_assoc($q2)){
								$name = $row['name'];
								$id = $row['ID'];
								//$type = $row['type'];
							}
						}*/
					?>
					<li><a href="userprofile.php?id=<?php echo $id; ?>"><span class="glyphicon glyphicon-user"></span> <?php echo $fname." ".$lname; ?></a></li>
					<li><a href="logout.php"><span class="glyphicon glyphicon-off"></span> Log Out</a></li>
				</span>
			</ul>
			</div>
			<?php
			}
			?>
		<div class="nav_bg">
			<div class="nav_bar"> <span></span> <span></span> <span></span> </div>
		</div>
    </nav>
	</div>