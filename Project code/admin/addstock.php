<?php
	include('../assets/config/config.php');
	error_reporting(0);
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
	include 'interface/head.php';
	?>
	<SCRIPT language=Javascript>
      <!--
		function isNumberKey(evt)
		{
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
		}
		
		function isPrice(evt, element) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		  if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode == 46))
			return false;
		  else {
			var len = $(element).val().length;
			var index = $(element).val().indexOf('.');
			//just allow 1 dot
			if (evt.target.value.search(/\./) > -1 && charCode == 46) {
                return false;
            }
			//only allow 2 number behind decimal
			if (index > 0 && charCode == 46) {
			  return false;
			}
			if (index > 0) {
			  var CharAfterdot = (len + 1) - index;
			  if (CharAfterdot > 3) {
				return false;
			  }
			}

		  }
		  return true;
		}
      //-->
	</SCRIPT>
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">Add Items</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<?php
					/*function check($date) {
					  if(preg_match("/^(\d{2})\/(\d{2})\/(\d{4})$/", $date, $matches))
						{
							if(checkdate($matches[2], $matches[1], $matches[3])){
								return true; 
							}
						}
					}*/
					if(isset($_POST['add'])){
						$num = $_POST['num'];
						for($i=0;$i<=$num;$i++){
						$name=$_POST['name'][$i];
						$quantity=$_POST['quantity'][$i];
						$quantity_type=$_POST['quantity_type'][$i];
						$cp=$_POST['cp'][$i];
						$sp=$_POST['sp'][$i];
						$category=$_POST['category'][$i];
						$date=$_POST['date'][$i];
						$image=$_FILES["images"]["name"][$i];
						$imageData=$_FILES["images"]["tmp_name"][$i];
						$description=$_POST['description'][$i];
						$query=mysql_query("SELECT * FROM stock WHERE name='".$name."'");
						$count=mysql_num_rows($query);
							if(empty($name) || empty($cp) || empty($sp) || empty($category))
							{
								$emp=true;
							}else{
								if($category!='sc'){
								if($image){
									if(($_FILES['images']['type'][$i]=='image/gif')
									|| ($_FILES['images']['type'][$i]=='image/jpeg')
									|| ($_FILES['images']['type'][$i]=='image/pjpeg')
									|| ($_FILES['images']['size'][$i]<200000)){
									if(move_uploaded_file($_FILES['images']['tmp_name'][$i],"../assets/images/".$_FILES['images']['name'][$i])){
									$mysql=mysql_query("INSERT INTO stock VALUES ('','$category','$name','0','$quantity_type','$image','$imageData','$cp','$sp',CURDATE(),'$description')");
									}
									}else{
										$image_type=true;
									}
								}else{
									$mysql=mysql_query("INSERT INTO stock VALUES ('','$category','$name','0','$quantity_type','$image','$imageData','$cp','$sp',CURDATE(),'$description')");
								}
								}else{
									$category_empty = true;
								}
							}
						}
						if($mysql){
							echo "<div class='alert alert-info'>".
								 "Successfully".
								"</div>";
						}else if($emp){
							echo "<div class='alert alert-danger'>".
								 "Please fill out all the fields".
								"</div>";
						}else if($category_empty){
							echo "<div class='alert alert-danger'>".
								"Please select category for this item.".
								"</div>";
						}else if($image_type){
							echo "<div class='alert alert-danger'>".
								"Image type must be GIF,JPG or PJPEG.".
								"</div>";
						}else{	
							echo"<div class='alert alert-danger'>".
								 "Something Wrong".
								"</div>";
						}
						
						//echo addslashes($_FILES["images"]["name"]);
					}
				?>
				<form method="post" action="addstock.php" enctype="multipart/form-data">
				<div class="form-inline" style="margin:0 0 1% 2.5%;">
					<label>Add</label>
					<input class="form-control" style="width:5%;" name="num" min="0" type="number" value="<?php echo $_POST['num']; ?>">
					<label>row</label>
					<input type="submit" class="btn btn-default" name="addrecord" value="Go">
				</div>
                <div class="row">
                    <!--<div class="panel panel-default" style="overflow-y:scroll;height:645px;">-->
                    <div class="panel panel-default">
					<?php
						if($_POST['submit']='addrecord'){
							$num = $_POST['num'];
							for($i=0;$i<=$num;$i++){
					?>
						<div class="panel-heading">
							New Items #<?php echo $i; ?>
						</div>
						<div class="panel-body">
							<!--<div class="form-group">
								<label>ID:</label>-->
								<input class="form-control" name="id[]" id="id" type="hidden">
							<!--</div>-->
						<div class="col-md-6">
							<div class="form-group">
								<label>Name:</label>
								<input class="form-control" name="name[]" id="name" type="text" placeholder="Item name">
							</div>
							<div class="form-group">
								<label>Date:</label>
								<input class="form-control" name="date[]" value="<?php echo date("Y-m-d");?>" id="date" type="date" disabled>
							</div>
							<div class="form-group">
								<label>Quantity:</label>
								<div class="input-group">
								<input class="form-control" name="quantity[]" id="quantity" type="number" onkeypress="return isNumberKey(event)" value="0" disabled>
									<div class="input-group-btn">
										<select class="form-control" name="quantity_type[]" style="width:150px;border-bottom-right-radius:4px;border-top-right-radius:4px;">
										<?php
											$query = mysql_query("SELECT * FROM quantitytype ORDER BY quantity_type ASC");
											while($row4=mysql_fetch_assoc($query)){
										?>
											<option value="<?php echo $row4['quantity_type'];?>"><?php echo $row4['quantity_type'];?></option>
										<?php
											}
										?>
										</select>
									</div>
								</div>
							</div>								
							<div class="form-group">
								<label>Cost:</label>
								<input class="form-control" name="cp[]" id="cp" type="text" onkeypress="return isPrice(event,this)" >
							</div>
							<div class="form-group">
								<label>Selling Price:</label>
								<input class="form-control" name="sp[]" id="sp" type="text" onkeypress="return isPrice(event,this)" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Image:</label>
								<label class="btn btn-block btn-default">
									Choose Image<input type="file" name="images[]" style="display: none;">
								</label>
							</div>
							<div class="form-group">
								<label>Category:</label>									
								<select class="form-control" name="category[]">
									<option name="sc" selected="selected" value="sc">--Select Category--</option>
								<?php
									$q = mysql_query("SELECT * FROM category ORDER BY categoryName ASC");
									while($row3=mysql_fetch_assoc($q)){
								?>
									<option value="<?php echo $row3['category_shortform']; ?>"><?php echo $row3['categoryName']; ?></option>
								<?php
									}
								?>
								</select>								
							</div>
							<div>
								<label for="add1">Desciption:</label>
								<textarea class="form-control" name="description[]" id="description"></textarea>
							</div>
						</div>
						</div>
					<?php
							}
						}
					?>
					<input type="submit" style="margin:-0.5% 0 1% 1%;" class="btn btn-info" id="add" name="add" value="Add Item">
					</div>
                </div>
				</form>
            </div>
        </div>
    </div>
</div>
				
<?php
	include 'interface/footer.php';
	}
?>