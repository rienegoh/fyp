<?php
	include('../assets/config/config.php');
	
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		
		if(!empty($_POST))
		{
			$post_data = $_POST['cbox'];
			$transport_id = $_POST['transport_id'];
			
			if(!empty($post_data)){
				
				$sql = "
					INSERT INTO shipping 
					(id , date , status , transportID) 
					select 
					'', '".date('Y-m-d')."' , 'active' , '".$transport_id."' 
				";
				$query4 = mysql_query($sql)or die(mysql_error());	
				$shipping_id = mysql_insert_id();
			
				foreach($post_data as $single_data)
				{
					$sql= "
						INSERT INTO shipping_details
						(
						id , shippingID , DOID , status 
						, address 
						)
						SELECT 
						'' , '".$shipping_id."' , '".$single_data."' , 'active' 
						, 
						(
							SELECT CONCAT(address1 , ',' , address2)  
							FROM customer c 
							INNER JOIN customer_delivery_order do ON do.customer_id = c.id 
							WHERE do.id = '".$single_data."'
						)
					";
					mysql_query($sql)or die(mysql_error());
					/* print $single_data[]; */
				}
				
			}
				header("location:viewtransport.php");
		}

		$sql = "
			SELECT 
			customer_delivery_order.date, 
			customer_delivery_order.DOID AS DOID, 
			customer.firstName ,customer.lastName , customer.phone, customer.email
			FROM customer_delivery_order 
			INNER JOIN customer ON customer_delivery_order.customer_id=customer.ID
			LEFT JOIN  shipping_details sd ON sd.doid = customer_delivery_order.id
			WHERE sd.id IS NULL
		";
	
	include 'interface/head.php';	
	$query4 = mysql_query($sql)or die(mysql_error());	

?>
	<script>
	function cal1(){
			var oneDay=24*60*60*1000;
			var fromDate=document.forms[0].from.value;
			var f = new Date(fromDate);
			var toDate=document.forms[0].to.value;
			var t = new Date(toDate);
			
			var result1=Math.round(Math.abs((f.getTime()-t.getTime())/(oneDay)));
			
			document.getElementById("totalDay").value=result1+" day";
		}
	
	
		$(document).ready(function () {
			$('#datatable').dataTable({
				"language": {
					"decimal": ",",
					"thousands": ".",
					"lengthMenu": "Show _MENU_ item per page",
					"zeroRecords": "Nothing found",
					"info": "Showing _START_ to _END_ of _TOTAL_ items",
					"infoEmpty": "No records available",
					"infoFiltered": "(filtered from _MAX_ total records)"
				},
				"order": [[ 0, "asc" ]],
				//"bLengthChange":false,
				//"pageLength": 12
				/*"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollY":        "800px",
				"scrollCollapse": true,
				"autoWidth": false,
				"sScrollX": "100%",
				"sScrollX": "visible: false",
				"aoColumnDefs": [
					{ "sWidth": "10%", "aTargets": [ -1 ] }
				]*/
			});							
		});
		jQuery('#dataTable').wrap('<div style="overflow:auto;" />');
	</script>
	<div id="page-wrapper">
        <div id="page-inner">


			<div class="row">
                <div class="col-md-12">
                    <div class="row">
						<div class="panel panel-default">
							<div class="panel-heading">
								Transport Detail
							</div>
							<div class="panel-body">
								<div class="table-responsive" style="overflow:auto;">
									<form method="POST" action="#">
									<table id="datatable" class="display cell-border table-bordered table-striped" style="width:%;border-bottom:1px solid #ddd;">
											

											<tr>
												<th></th>
												<th>DO No</th>
												<th>Customer</th>
												<th>Contact No</th>
												<th>Email Address</th>
												<th>Delivery Date</th>
												<th>View DO</th>
											</tr>
<?php
$no=1;
$i=1;
while($row = mysql_fetch_assoc($query4)){
?>
											<tr>
													<td><input type="checkbox" name="cbox[]" value="<?php echo $row['DOID']; ?>"></td>
													<td><?php echo "CD".$row['DOID']; ?></td>
													<td><?php echo $row['firstName']." ".$row['lastName']; ?></td>
													<td><?php echo $row['phone'];?></td>
													<td><?php echo $row['email'];?></td>
													<td><?php echo date("d-m-Y",strtotime($row['date']));?></td>
													<td><a href="view_customer_do.php?doid=<?php echo $row['DOID'];?>" class="btn btn-primary btn-outline"><i class="fa fa-eye" aria-hidden="true">&nbsp;</i>View DO</a></td>
												</tr>
								
<?php
$i++;
$no++;
}
?>
											<input type="hidden" name="transport_id" value="<?php print $_GET['transport_id'] ?>">
									</table>
									</br>
									<input class="btn btn-success" type="submit" name="submit" value="Submit">
									<input class="btn btn-primary" type="button" onclick="history.back();" value="Back">
									</form>
								</div>
							</div>	
						</div>		
						
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<?php
	include 'interface/footer.php';
	}
?>