<?php
	include('../assets/config/config.php');
	
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
	<SCRIPT language=Javascript>
      <!--
		function isNumberKey(evt)
		{
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
		}
		
		function isNumber(evt, element) {

			var charCode = (evt.which) ? evt.which : event.keyCode

			if (
				(charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
				(charCode < 48 || charCode > 57)){
				return false;
				}else{
				return true;
				}
		}
      //-->
	</SCRIPT>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Edit Staff</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
							<div class="panel panel-default">
								<div class="panel-heading">
									Staff Detail
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<?php
											
											if(isset($_GET['edit'])){
												
												$id=filter_var($_GET['edit'],FILTER_SANITIZE_NUMBER_INT);
												$query2=mysql_query("SELECT * FROM supplier WHERE ID=$id")or die(mysql_error());
												
												while ($row2=mysql_fetch_assoc($query2)){
													$supp_id=$row2['ID'];
													$cname=$row2['company_name'];
													$c_iso=$row2['country_iso'];
													$cphone=$row2['company_phone'];
													$pic=$row2['person_in_charge'];
													$email=$row2['email'];
													/*----phone----*/
													//$phone=$row2['phone'];
													$phone=explode('-', $row2['phone'], 2);
													$countph=count($phone);
													//$type=$row2['type'];
													/*----Address----*/
													$addr=explode('|', $row2['address'], 2);
													$count=count($addr);
												}
											}
										?>
										<div class="col-md-6">
										<form role="form" action="supplier.php?action=update&id=<?php echo $supp_id;?>&name=<?php echo $pic;?>" method="POST">
											<div>
												<input type="hidden" name="supp_id" value="<?php echo $supp_id; ?>">
												<label for="cname">Company Name:</label>
												<input type="text" class="form-control" name="company_name" value="<?php echo $cname; ?>">
											</div>
											<div>
												<label for="phone">Country:</label>
												<select class="form-control" name="select_country">
													<option selected="selected" value="sc">--Select Country--</option>
												<?php
													$q = mysql_query("SELECT * FROM country ORDER BY nicename ASC");
													while($row2=mysql_fetch_assoc($q)){
												?>
													<option value="<?php echo $row2['iso']; ?>" <?php if($c_iso==$row2['iso']){?>selected="selected"<?php } ?>><?php echo $row2['nicename']." &rlm;"."(&lrm;+".$row2['phonecode'].")"; ?></option>
												<?php
													}
												?>
												</select>
											</div>
											<div>
												<label for="cname">Company Phone:</label>
												<input type="text" class="form-control" name="company_phone" value="<?php echo $cphone; ?>" maxlength="10" placeholder="E.g. 07-0000000" onkeypress="return isNumber(event,this)" required>
											</div>
											<div>
												<label for="cname">Person In Charge:</label>
												<input type="text" class="form-control" name="pic"  value="<?php echo $pic; ?>">
											</div>
											</br>
											<input type="submit" class="btn btn-info" name="update" value="Update">
											<input type="reset" class="btn btn-info" value="Reset">
										</div>
										<div class="col-md-6">
											<div>
												<label for="phone">Phone no:</label>
												<div class="input-group">
													<input type="text" class="form-control" onkeypress="return isNumber(event)" value="<?php if($countph>0){echo $phone[0];} ?>" name="phone" maxlength="3" style="z-index:0;">
													<span class="input-group-addon">-</span>
													<input type="text" class="form-control" onkeypress="return isNumber(event)" value="<?php if($countph>1){echo $phone[1];} ?>" name="phone1" maxlength="7" style="z-index:0;">
												</div>
											</div>
											<div>
												<label for="cname">Email:</label>
												<input type="text" class="form-control" name="supplier_email" value="<?php echo $email; ?>">
											</div>
											<div>
												<label for="add1">Address1:</label>
												<input type="text" class="form-control" name="address1" id="address1" 
												value="<?php 
													//echo $addr[0];for($i=1;$i<$count;$i++){echo ", ".$addr[$i];}
													echo $addr[0];
												?>">
											</div>
											<div>
												<label for="add1">Address2:</label>
												<input type="text" class="form-control" name="address2" id="address2" 
												value="<?php 
													//echo $addr[0];for($i=1;$i<$count;$i++){echo ", ".$addr[$i];}
													if($count>1){
														echo $addr[1];
													}
												?>">
											</div>
										</div>
										</form>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
	include 'interface/footer.php';
	}
?>