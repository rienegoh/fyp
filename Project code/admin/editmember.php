<?php
	include('../assets/config/config.php');
	
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
	<SCRIPT language=Javascript>
      <!--
		function isNumberKey(evt)
		{
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
		}
		
		function isNumber(evt, element) {

			var charCode = (evt.which) ? evt.which : event.keyCode

			if (
				(charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
				(charCode < 48 || charCode > 57)){
				return false;
				}else{
				return true;
				}
		}
      //-->
	</SCRIPT>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Edit Member</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
							<div class="panel panel-default">
								<div class="panel-heading">
									Member Detail
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<?php
											$id="";
											$fname="";
											$lname="";
											$ic="";
											$dob="";
											$email="";
											$phone="";
											$gender="";
											$addr1="";
											$addr2="";
											$cname="";
											$cphone="";
											if(isset($_GET['edit'])){
												
												$id=filter_var($_GET['edit'],FILTER_SANITIZE_NUMBER_INT);
												$query2=mysql_query("SELECT * FROM customer WHERE ID=$id")or die(mysql_error());
												
												while ($row2=mysql_fetch_assoc($query2)){
													$id=$row2['ID'];
													$fname=$row2['firstName'];
													$lname=$row2['lastName'];
													//$ic=$row2['nric'];
													//$dob=$row2['dob'];
													$email=$row2['email'];
													$c_iso=$row2['country_iso'];
													/*----phone----*/
													$phone=explode('-', $row2['phone'], 2);
													$countph=count($phone);
													//$phone=$row2['phone'];
													$gender=$row2['gender'];
													$addr1=$row2['address1'];
													$addr2=$row2['address2'];
													$cname=$row2['cname'];
													/*----company phone----*/
													$cphone=explode('-', $row2['cphone'], 2);
													$countcph=count($cphone);
													//$cphone=$row2['cphone'];
													$username=$row2['username'];
													$password=$row2['password'];
													//$type=$row2['type'];
												}
											}
										?>
											<div class="col-md-6">
										<form role="form" action="member.php?action=update&id=<?php echo $id;?>&name=<?php echo $fname." ".$lname;?>" method="POST" accept-charset="utf-8">
											<div>
												<input type="hidden" name="id" value="<?php echo $id; ?>">
												<label for="name">Name:</label>
												<div class="input-group">
													<span class="input-group-addon">First Name</span>
													<input type="text" class="form-control" name="fname" value="<?php echo $fname; ?>" style="z-index:0;">
													<span class="input-group-addon">Last Name</span>
													<input type="text" class="form-control" name="lname" value="<?php echo $lname; ?>" style="z-index:0;">
												</div>
											</div>
											<div>
												<label for="fname">Username:</label>
												<input type="text" class="form-control" name="username" value="<?php echo $username; ?>" disabled>
											</div>
											<div>
												<label for="fname">Password:</label>
												<input type="text" class="form-control" name="password" value="<?php echo $password; ?>">
											</div>
											<div>
												<label for="email">Email: </label>
												<input type="email" class="form-control" name="email" value="<?php echo $email; ?>" placeholder="Enter email">
											</div>
											<div>
												<label for="country">Country:</label>
												&nbsp;&nbsp;
												<select class="form-control" name="country">
												<?php
													$q = mysql_query("SELECT * FROM country ORDER BY nicename ASC");
													while($row3=mysql_fetch_assoc($q)){
												?>
													<option value="<?php echo $row3['iso']; ?>" <?php if($c_iso==$row3['iso']){?>selected="selected"<?php } ?>><?php echo $row3['nicename']." &rlm;"."(&lrm;+".$row3['phonecode'].")"; ?></option>
												<?php
													}
												?>
												</select>
											<div>
												<label for="gender">Gender :</label>
												<div>
												<label class="radio-inline">
													<input type="radio" name="gender" <?php if($gender=="Male"){ ?>checked="checked" <?php } ?> value="Male">Male
												</label>
												<label class="radio-inline">
													<input type="radio" name="gender" <?php if($gender=="Female"){ ?>checked="checked" <?php } ?> value="Female">Female
												</label>
												</div>
											</div>
											</br>
											<input type="submit" class="btn btn-info" name="update" value="Update">
											<input type="reset" class="btn btn-info" value="Reset">
											</div>
											</div>
											<div class="col-md-6">
											<div>
												<label for="phone">Phone:</label>
												<div class="input-group">
													<input type="text" class="form-control" onkeypress="return isNumber(event)" value="<?php if($countph>0){echo $phone[0];} ?>" name="phone" maxlength="3" style="z-index:0;">
													<span class="input-group-addon">-</span>
													<input type="text" class="form-control" onkeypress="return isNumber(event)" value="<?php if($countph>1){echo $phone[1];} ?>" name="phone1" maxlength="7" style="z-index:0;">
												</div>
											</div>
											<!--<div>
												<label for="type">User Type:</label>
												<select class="form-control" name="type">
												<?php
													/*$q = mysql_query("SELECT * FROM user_type ORDER BY usertype ASC");
													while($row4=mysql_fetch_assoc($q)){
												?>
													<option value="<?php echo $row4['type_id']; ?>" <?php if($type==$row4['type_id']){?>selected="selected"<?php } ?>><?php echo $row4['usertype']; ?></option>
												<?php 
													} */
												?>
												</select>
											</div>-->
											<div>
												<label for="address">Address 1:</label>
												<input type="text" class="form-control" name="addr1" value="<?php echo $addr1; ?>">
												<!--<textarea class="form-control" name="address"><?php echo $addr; ?></textarea>-->
											</div>
											<div>
												<label for="address">Address 2:</label>
												<input type="text" class="form-control" name="addr2" value="<?php echo $addr2; ?>">
											</div>
											<div>
												<label for="cname">Company Name:</label>
												<input type="text" class="form-control" name="cname" value="<?php echo $cname; ?>" >
											</div>
											<div>
												<label for="cphone">Company Phone:</label>
												<div class="input-group">
													<input type="text" class="form-control" onkeypress="return isNumber(event)" value="<?php if($countcph>0){echo $cphone[0];} ?>" name="cphone" maxlength="2" style="z-index:0;">
													<span class="input-group-addon">-</span>
													<input type="text" class="form-control" onkeypress="return isNumber(event)" value="<?php if($countcph>1){echo $cphone[1];} ?>" name="cphone1" maxlength="7" style="z-index:0;">
												</div>
											</div>
											</div>
										</form>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
	include 'interface/footer.php';
	}
?>