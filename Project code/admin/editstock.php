<?php
	include('../assets/config/config.php');
	
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
	<SCRIPT language=Javascript>
      <!--
      function isNumberKey(evt)
		{
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
		}
		function isPrice(evt, element) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		  if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode == 46 || charcode == 8))
			return false;
		  else {
			var len = $(element).val().length;
			var index = $(element).val().indexOf('.');
			//just allow 1 dot
			if (evt.target.value.search(/\./) > -1 && charCode == 46) {
                return false;
            }
			//only allow 2 number behind decimal
			if (index > 0 && charCode == 46) {
			  return false;
			}
			if (index > 0) {
			  var CharAfterdot = (len + 1) - index;
			  if (CharAfterdot > 3) {
				return false;
			  }
			}

		  }
		  return true;
		}
      //-->
	</SCRIPT>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Edit Item</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
							<div class="panel panel-default">
								<div class="panel-heading">
									Item Detail
								</div>
								<div class="panel-body">
										<?php
											$id="";
											$category_prefix="";
											$name="";
											$quantity="";
											$cp="";
											$sp="";
											$category="";
											if(isset($_GET['edit'])){
												$id=filter_var($_GET['edit'],FILTER_SANITIZE_NUMBER_INT);
												$query2=mysql_query(
												"SELECT 
												stock.ID,stock.category_prefix,stock.name,stock.quantity,stock.companyPrice,stock.sellingPrice,
												stock.image,stock.image_data,stock.description,stock.date,stock.quantity_type,category.categoryName
												FROM stock INNER JOIN category ON stock.category_prefix = category.category_shortform 
												WHERE ID='$id'")or die(mysql_error());
												
												while ($row2=mysql_fetch_assoc($query2)){
													$id=$row2['ID'];
													$category_prefix=$row2['category_prefix'];
													$name=$row2['name'];
													$quantity=$row2['quantity'];
													$quantity_type=$row2['quantity_type'];
													$cp=$row2['companyPrice'];
													$sp=$row2['sellingPrice'];
													//$category_prefix=$row2['category_prefix'];
													$category=$row2['categoryName'];
													$img=$row2['image'];
													$img_tmp=$row2['image_data'];
													$date=$row2['date'];
													$description=$row2['description'];
													//echo date("m-d-Y",strtotime($date));
												}
											}
										?>
									<div class="col-md-12">
									<div class="col-md-6">
										<form enctype="multipart/form-data" method="post" action="inventory.php?action=edit&id=<?php echo $id;?>&category_prefix=<?php echo $category_prefix;?>&name=<?php echo $name;?>">
											<!--<div class="form-group">
												<label>ID:</label>-->
												<input class="form-control" name="id" id="id" type="hidden" value="<?php echo $id;?>">
												<input class="form-control" name="category_prefix" id="category_prefix" type="hidden" value="<?php echo $category_prefix;?>">
											<!--</div>-->
											<div class="form-group">
												<label>Name:</label>
												<input class="form-control" name="name" id="name" type="text" value="<?php echo $name; ?>">
											</div>
											<div class="form-group">
												<label>Last Update Date:</label>
												<input class="form-control" name="date" value="<?php echo date("Y-m-d",strtotime($date));?>" id="date" type="date" disabled>
											</div>
											<div class="form-group">
												<label>Quantity:</label>
												<div class="input-group">
													<input class="form-control" name="quantity" id="quantity" type="text" value="<?php echo $quantity; ?>" onkeypress="return isNumberKey(event)" disabled>
												
													<div class="input-group-btn">
														<select class="form-control" name="quantity_type" style="width:150px;border-bottom-right-radius:4px;border-top-right-radius:4px;">
														<?php
															$query = mysql_query("SELECT * FROM quantitytype ORDER BY quantity_type ASC");
															while($row4=mysql_fetch_assoc($query)){
														?>
															<option value="<?php echo $row4['quantity_type'];?>" <?php if($quantity_type==$row4['quantity_type']){?> selected <?php }?>><?php echo $row4['quantity_type'];?></option>
														<?php
															}
														?>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label>Category:</label>
												<select class="form-control" name="category">
												<?php
													$q = mysql_query("SELECT * FROM category ORDER BY categoryName ASC");
													while($row3=mysql_fetch_assoc($q)){
												?>
													<option value="<?php echo $row3['category_shortform']; ?>"<?php if($category_prefix==$row3['category_shortform']){?>selected="selected"<?php } ?>><?php echo $row3['categoryName']; ?></option>
												<?php
													}
												?>
												</select>
											</div>
											<input type="submit" class="btn btn-info" id="update" name="update" value="Update">
									</div>
									<div class="col-md-6">
											<div class="form-group">
												<label>Company Price:</label>
												<input class="form-control" name="cp" id="cp" type="text" value="<?php echo $cp; ?>" onkeypress="return isPrice(event,this)">
											</div>
											<div class="form-group">
												<label>Selling Price:</label>
												<input class="form-control" name="sp" id="sp" type="text" value="<?php echo $sp; ?>" onkeypress="return isPrice(event,this)">
											</div>
											<div class="form-group">
												<label>Image:</label>
												<label class="btn btn-block btn-default">
													Choose Image<input type="file" name="images" style="display: none;">
												</label>
											</div>
											<div>
												<label for="add1">Desciption:</label>
												<textarea class="form-control" name="description" id="description"><?php echo $description; ?></textarea>
											</div>
									</div>
										</form>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
	include 'interface/footer.php';
	}
?>