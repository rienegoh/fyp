<?php
	include('../assets/config/config.php');
	
	error_reporting(0);
	
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		
		include 'interface/head.php';
	
	/*---------------------------------Update---------------------------------------------*/
		if(isset($_POST['update'])){
			$id=$_POST['supp_id'];
			$cname=$_POST['company_name'];
			$country=$_POST['select_country'];
			$cphone=$_POST['company_phone'];
			$pic=$_POST['pic'];
			$phone=$_POST['phone']."-".$_POST['phone1'];
			$email=$_POST['supplier_email'];
			$address=$_POST['address1']."|".$_POST['address2'];
			$query3=mysql_query("UPDATE supplier SET company_name='$cname',country_iso='$country',company_phone='$cphone',
			person_in_charge='$pic',phone='$phone',email='$email',address='$address' WHERE ID=$id");
			//header("location:member.php?action=update&id=$id&name=$name");
		}
	/*---------------------------------Update End---------------------------------------------*/
	/*---------------------------------Checkbox Delete---------------------------------------------*/
	if($_POST['submit']='delete'){
		$rowCount = count($_POST["checkbox"]);
		//$_POST[""];
		for($i=0;$i<$rowCount;$i++) {
			$d = mysql_query("DELETE FROM supplier WHERE ID='" . $_POST["checkbox"][$i] . "'");
			$delete_check = true;
		}
		/*echo '<pre>';
		print_r($_POST);
		echo '</pre>';*/
	}
	/*---------------------------------Checkbox Delete End---------------------------------------------*/
?>
	<script>
		$(document).ready(function () {
			$('#datatable').dataTable({
				"language": {
					"decimal": ",",
					"thousands": ".",
					"lengthMenu": "Show _MENU_ supplier per page",
					"zeroRecords": "Nothing found",
					"info": "Showing _START_ to _END_ of _TOTAL_ supplier",
					"infoEmpty": "No records available",
					"infoFiltered": "(filtered from _MAX_ total records)"
				},
				"order": [[ 1, "asc" ]],
				//"bLengthChange":false,
				//"pageLength": 10
				/*"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollY":        "800px",
				"scrollCollapse": true,
				"autoWidth": false,
				"sScrollX": "100%",
				"aoColumnDefs": [
					{ "sWidth": "10%", "aTargets": [ -1 ] }
				]*/
			});							
		});
	</script>
    <div id="page-wrapper">
        <div id="page-inner">
			<?php
				$action = isset($_GET['action']) ? $_GET['action'] : "";
				$name = isset($_GET['name']) ? $_GET['name'] : "";
					 
				if($delete_check==true){
					echo "<div class='alert alert-info'>";
					echo "Delete Successful!";
					echo "</div>";
				}else if($query3){
					echo "<div class='alert alert-info'>";
					echo "<strong>{$name}</strong> detail was updated!";
					echo "</div>";
				}
			?>
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">Supplier Manage</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-md-12">
                    <div class="row">
						<form id="form" method="post" action="#">
						<div class="panel panel-default">
							<div class="panel-heading">
								Supplier Detail
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table id="datatable" width="100%" class="display cell-border table-bordered table-striped" style="border-bottom:1px solid #ddd;">
										<thead>
											<tr>
												<th>
												<center>
													<div class="checkbox">
														<input type="checkbox" id="checkall" onchange="checkall()">
													</div>
												</center>
												</th>
												<!--<th>ID</th>-->
												<th>Compony Name</th>
												<th>Compony Contact No.</th>
												<th>Contact No</th>
												<th>Person In Charge</th>
												<th>Email</th>
												<th>Edit</th>
												<th>Create PO</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$query = mysql_query("SELECT * FROM supplier INNER JOIN country ON supplier.country_iso=country.iso")or die(mysql_error());
											while($row = mysql_fetch_assoc($query)){
										?>
											<tr>
												<td>
												<center>
													<div class="checkbox">
														<input type="checkbox" id="checkbox[]" class="group" name="checkbox[]" value="<?php echo $row['ID']; ?>">
													</div>
												</center>
												</td>
												<!--<td><?php //echo 'S'.$row['ID']; ?></td>-->
												<td><?php echo $row['company_name']; ?></td>
												<td><?php echo $row['company_phone']; ?></td>
												<td><?php echo "(&lrm;+".$row['phonecode'].")".$row['phone']; ?></td>
												<td><?php echo $row['person_in_charge']; ?></td>
												<td><?php echo $row['email']; ?></td>
												<td><a href="editsupplier.php?edit=<?php echo $row['ID'];?>" class="btn btn-primary"><i class="glyphicon glyphicon-edit">&nbsp;</i>Edit</a></td>
												<td><a href="createPO.php?id=<?php echo $row['ID'];?>" class="btn btn-info"><i class="fa fa-plus-square" aria-hidden="true">&nbsp;</i>Create PO</a></td>
											</tr>
										<?php
											}
										?>
										</tbody>
									</table>
									<!--Delete All-->
									<button type="submit" name="delete" title="delete" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?');" value="delete">
									<i class="fa fa-trash"></i> Delete Selected
									</button>
								</div>
							</div>	
						</div>		
						</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
	include 'interface/footer.php';
	}
?>