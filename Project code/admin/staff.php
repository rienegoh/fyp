<?php
	include('../assets/config/config.php');
	
	error_reporting(0);
	
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		
		include 'interface/head.php';
	
	/*---------------------------------Update---------------------------------------------*/
		if(isset($_POST['update'])){
			$id=$_POST['id'];
			$username=$_POST['username'];
			$password=trim($_POST['password']);
			$name=$_POST['name'];
			$email=$_POST['email'];
			$dob=$_POST['dob'];
			$ic = $_POST['ic'];
			$ic1 = $_POST['ic1'];
			$ic2 = $_POST['ic2'];
			$final_ic = $ic."-".$ic1."-".$ic2;
			$phone=$_POST['phone'];
			$phone1=$_POST['phone1'];
			$ph=$phone."-".$phone1;
			$address1=$_POST['address1'];
			$address2=$_POST['address2'];
			$address=$address1."|".$address2;
			$gender=$_POST['gender'];
			$hire_date=$_POST['hire_date'];
			$basic_salary=$_POST['basic_salary'];
			//$type=$_POST['type'];
			
			$query3=mysql_query(
			"UPDATE staff SET password='$password',firstName='$fname',lastName='$lname',email='$email',dob='$dob',ic='$final_ic',
			phone='$ph',gender='$gender',address='$address',hire_date='$hire_date',basic_salary='$basic_salary' WHERE ID='$id'" );
			//header("location:member.php?action=update&id=$id&name=$name");
		}
	/*---------------------------------Update End---------------------------------------------*/
	/*---------------------------------Checkbox Delete---------------------------------------------*/
	if($_POST['submit']='delete'){
		$rowCount = count($_POST["checkbox"]);
		//$_POST[""];
		for($i=0;$i<$rowCount;$i++) {
			$d = mysql_query("DELETE FROM staff WHERE ID='" . $_POST["checkbox"][$i] . "'");
			$delete_check = true;
		}
		/*echo '<pre>';
		print_r($_POST);
		echo '</pre>';*/
	}
	/*---------------------------------Checkbox Delete End---------------------------------------------*/
?>
	<script>
		$(document).ready(function () {
			$('#datatable').dataTable({
				"language": {
					"decimal": ",",
					"thousands": ".",
					"lengthMenu": "Show _MENU_ staff per page",
					"zeroRecords": "Nothing found",
					"info": "Showing _START_ to _END_ of _TOTAL_ staffs",
					"infoEmpty": "No records available",
					"infoFiltered": "(filtered from _MAX_ total records)"
				},
				"order": [[ 1, "asc" ]],
				//"bLengthChange":false,
				//"pageLength": 10
				/*"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollY":        "800px",
				"scrollCollapse": true,
				"autoWidth": false,
				"sScrollX": "100%",
				"aoColumnDefs": [
					{ "sWidth": "10%", "aTargets": [ -1 ] }
				]*/
			});							
		});
	</script>
    <div id="page-wrapper">
        <div id="page-inner">
			<?php
				$action = isset($_GET['action']) ? $_GET['action'] : "";
				$name = isset($_GET['name']) ? $_GET['name'] : "";
					 
				if($delete_check==true){
					echo "<div class='alert alert-info'>";
					echo "Delete Successful!";
					echo "</div>";
				}else if($query3){
					echo "<div class='alert alert-info'>";
					echo "<strong>{$name}</strong> detail was updated!";
					echo "</div>";
				}
			?>
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">Staff Manage</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-md-12">
                    <div class="row">
						<form id="form" method="post" action="#">
						<div class="panel panel-default">
							<div class="panel-heading">
								Staff Detail
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table id="datatable" width="100%" class="display cell-border table-bordered table-striped" style="border-bottom:1px solid #ddd;">
										<thead>
											<tr>
												<th>
												<center>
													<div class="checkbox">
														<input type="checkbox" id="checkall" onchange="checkall()">
													</div>
												</center>
												</th>
												<!--<th>ID</th>-->
												<th>Name</th>
												<th>Email</th>
												<th>Birthdate</th>
												<th>Contact No</th>
												<th>I/C</th>
												<th>Gender</th>
												<th>Address</th>
												<!--<th>Hire date</th>-->
												<th>Salary</th>
												<th>Edit</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$query = mysql_query("SELECT * FROM staff")or die(mysql_error());
											while($row = mysql_fetch_assoc($query)){
										?>
											<tr>
												<td>
												<center>
													<div class="checkbox">
														<input type="checkbox" id="checkbox[]" class="group" name="checkbox[]" value="<?php echo $row['ID']; ?>">
													</div>
												</center>
												</td>
												<!--<td><?php //echo 'A'.$row['ID']; ?></td>-->
												<td><?php echo $row['firstName']." ".$row['lastName']; ?></td>
												<td><?php echo $row['email']; ?></td>
												<td><?php echo $row['dob']; ?></td>
												<td><?php echo $row['phone']; ?></td>
												<td><?php echo $row['ic']; ?></td>
												<td><?php echo $row['gender']; ?></td>
												<td><?php
													//echo $row['address']; 
													$addr=explode('|', $row['address'], 2);
													$count=count($addr);
													if($count>0){
														echo $addr[0];
														for($i=1;$i<$count;$i++){
															echo ", ".$addr[$i];
														}
													}
												?></td>
												<!--<td><?php echo $row['hire_date']; ?></td>-->
												<td><?php echo 'RM '.number_format($row['basic_salary'],2); ?></td>
												<td><a href="editstaff.php?edit=<?php echo $row['ID'];?>" class="btn btn-primary"><i class="glyphicon glyphicon-edit">&nbsp;</i>Edit</a></td>
											</tr>
										<?php
											}
										?>
										</tbody>
									</table>
									<!--Delete All-->
									<button type="submit" name="delete" title="delete" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?');" value="delete">
									<i class="fa fa-trash"></i> Delete Selected
									</button>
								</div>
							</div>	
						</div>		
						</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
	include 'interface/footer.php';
	}
?>