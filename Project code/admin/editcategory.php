<?php
	include('../assets/config/config.php');
	
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
	<SCRIPT language=Javascript>
      <!--
      function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
      //-->
	</SCRIPT>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Edit Item</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
							<div class="panel panel-default">
								<div class="panel-heading">
									Item Detail
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<?php
											$id="";
											$csf="";
											$cname="";
											$des="";
											if(isset($_GET['edit'])){
												
												$id=$_GET['edit'];
												$query2=mysql_query("SELECT * FROM category WHERE CID='$id'")or die(mysql_error());
												
												while ($row2=mysql_fetch_assoc($query2)){
													$id=$row2['CID'];
													$csf=$row2['category_shortform'];
													$cname=$row2['categoryName'];
													$des=$row2['description'];
													$img=$row2['image'];
													$img_tmp=$row2['image_data'];
												}
											}
										?>
										<form method="post" enctype="multipart/form-data" action="category.php?action=update&id=<?php echo $id;?>&name=<?php echo $cname;?>">
											<input type="hidden" name="cid" value="<?php echo $id;?>">
											<div class="form-group">
												<label>Category Short Form:</label>
												<input class="form-control" name="category_shortform" id="category_shortform" type="text" value="<?php echo $csf;?>">
											</div>
											<div class="form-group">
												<label>Category Name:</label>
												<input class="form-control" name="category_name" id="category_name" type="text" value="<?php echo $cname;?>">
											</div>
											<div class="form-group">
													<label>Image:</label>
													<input type="file" name="images"></br>
											</div>
											<div class="form-group">
												<label>Description:</label>
												<textarea class="form-control" name="description" id="description"><?php echo $des;?></textarea>
											</div>
											<input type="submit" class="btn btn-info" id="update" name="update" value="Update">
										</form>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
	include 'interface/footer.php';
	}
?>