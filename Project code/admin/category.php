<?php
	include('../assets/config/config.php');
	
	error_reporting(0);
	
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		
		include 'interface/head.php';
	
	/*---------------------------------Update---------------------------------------------*/
		if(isset($_POST['update'])){
			$id=$_POST['cid'];
			$csf=$_POST['category_shortform'];
			$cname=$_POST['category_name'];
			$img=$_FILES['images']['name'];
			$img_tmp=$_FILES['images']['tmp_name'];
			$des=$_POST['description'];
			if($img){
				if(($_FILES['images']['type'][$i]=='image/gif')
				|| ($_FILES['images']['type'][$i]=='image/jpeg')
				|| ($_FILES['images']['type'][$i]=='image/pjpeg')
				|| ($_FILES['images']['size'][$i]<200000)){
				move_uploaded_file($img_tmp,"../assets/images/".$img);
				$query3=mysql_query("UPDATE category SET category_shortform='$csf',categoryName='$cname',image='$img',image_data='$img_tmp',description='$des' WHERE CID='$id'" );
				}else{
					$image_type=true;
				}
			}else{
				$query3=mysql_query("UPDATE category SET category_shortform='$csf',categoryName='$cname',image='demoUpload.jpg',image_data='$img_tmp',description='$des' WHERE CID='$id'" );
			}
			//header("location:member.php?action=update&id=$id&name=$name");
		}
	/*---------------------------------Update End---------------------------------------------*/
	/*---------------------------------Checkbox Delete---------------------------------------------*/
	if($_POST['submit']='delete'){
		$rowCount = count($_POST["checkbox"]);
		//$_POST[""];
		for($i=0;$i<$rowCount;$i++) {
			$d = mysql_query("DELETE FROM category WHERE category_shortform='" . $_POST["checkbox"][$i] . "'");
			$delete_check = true;
		}
		/*echo '<pre>';
		print_r($_POST);
		echo '</pre>';*/
	}
	/*---------------------------------Checkbox Delete End---------------------------------------------*/
?>
	<script>
		$(document).ready(function () {
			$('#datatable').dataTable({
				"language": {
					"decimal": ",",
					"thousands": ".",
					"lengthMenu": "Show _MENU_ category per page",
					"zeroRecords": "Nothing found",
					"info": "Showing _START_ to _END_ of _TOTAL_ category",
					"infoEmpty": "No records available",
					"infoFiltered": "(filtered from _MAX_ total records)"
				},
				"order": [[ 1, "asc" ]],
				//"bLengthChange":false
				/*"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollY":        "800px",
				"scrollCollapse": true,
				"autoWidth": false,
				"sScrollX": "100%",
				"aoColumnDefs": [
					{ "sWidth": "10%", "aTargets": [ -1 ] }
				]*/
			});							
		});
	</script>
    <div id="page-wrapper">
        <div id="page-inner">
			<?php
				$action = isset($_GET['action']) ? $_GET['action'] : "";
				$name = isset($_GET['name']) ? $_GET['name'] : "";
					 
				if($delete_check==true){
					echo "<div class='alert alert-info'>";
					echo "Delete Successful!";
					echo "</div>";
				}else if($action=='update'){
					echo "<div class='alert alert-info'>";
					echo "<strong>{$name}</strong> detail was updated!";
					echo "</div>";
				}
			?>
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">Category Manage</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-md-12">
                    <div class="row">
						<form id="form1" method="post" action="#">
						<div class="panel panel-default">
							<div class="panel-heading">
								Category Detail
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table id="datatable" class="display cell-border table-bordered table-striped" style="border-bottom:1px solid #ddd;">
										<thead>
											<tr>
												<th style="width:5%;">
													<div class="checkbox">
														<input type="checkbox" id="checkall" onchange="checkall()">
													</div>
												</th>
												<th width="15%">Short Form</th>
												<th>Category Name</th>
												<th>Description</th>
												<th width="5%">Edit</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$query = mysql_query("SELECT * FROM category")or die(mysql_error());
											while($row = mysql_fetch_assoc($query)){
										?>
											<tr>
												<td>
												<center>
													<div class="checkbox">
														<input type="checkbox" id="checkbox[]" class="group" name="checkbox[]" value="<?php echo $row['category_shortform']; ?>">
													</div>
												</center>
												</td>
												<td style="text-align:center"><?php echo $row['category_shortform']; ?></td>
												<td><?php echo $row['categoryName']; ?></td>
												<td><?php echo $row['description']; ?></td>
												<td><a href="editcategory.php?edit=<?php echo $row['CID'];?>" class="btn btn-primary"><i class="glyphicon glyphicon-edit">&nbsp;</i>Edit</a></td>
											</tr>
										<?php
											}
										?>
										</tbody>
									</table>
									<!--Delete All-->
									<button type="submit" name="delete" title="delete" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?');" value="delete">
									<i class="fa fa-trash"></i> Delete Selected
									</button>
								</div>
							</div>	
						</div>		
						</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
	include 'interface/footer.php';
	}
?>