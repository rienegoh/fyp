<?php
	include('../assets/config/config.php');
	error_reporting(0);
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
	<script>
		/*$(document).ready(function () {
			$('#datatable').dataTable({
				"language": {
					"decimal": ",",
					"thousands": ".",
					"lengthMenu": "Show _MENU_ supplier per page",
					"zeroRecords": "Nothing found",
					"info": "",
					"infoEmpty": "No records available",
					"infoFiltered": "(filtered from _MAX_ total records)"
				},
				"ordering": false,
				"bFilter":false,
				"paging":   false,
				//"order": [[ 0, "asc" ]],
				"bLengthChange":false,
				//"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollCollapse": true,
				"autoWidth": false,
				"sScrollX": "100%",
				"sScrollX": "visible: false"
			});							
		});
		jQuery('#dataTable').wrap('<div style="overflow:auto;" />');*/
	</script>

	
	<div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
					<h1 class="page-head-line">Delivery Order</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-md-12">	
                    <div class="row">
					<form method="post" action="#">
					<div class="panel panel-default">
						<div class="panel-heading">
							Details
						</div>
						<div class="panel-body">
						<?php
							if(isset($_GET['doid'])){
								$ID=$_GET['doid'];
								$query2=mysql_query("SELECT * FROM delivery_order INNER JOIN supplier ON delivery_order.supplier_id=supplier.ID WHERE DOID='$ID'")or die(mysql_error());
								$countq2=mysql_num_rows($query2);
								if($countq2=1){
								while($row = mysql_fetch_assoc($query2)){
									$supplier_id=$row['supplier_id'];
									$poid=$row['po_id'];
									$doid=$row['DOID'];
									$name=$row['person_in_charge'];
									$compn=$row['company_name'];
									$phone=$row['phone'];
									$cphone=$row['company_phone'];
									$address=$row['address'];
									$email=$row['email'];
									$date=$row['date'];
								}
								}else{
									$supplier_id='';
									$poid='';
									$name='';
									$compn='';
									$phone='';
									$cphone='';
									$address='';
									$email='';
									$date='';
								}
							}	
						?>
							<div class="printsize" style="float:left;position:relative;">
							<label><b><u>Supplier Detail</b></u></label>
							<table class="adjusttd">
								<tr>
									<td>Supplier ID:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $supplier_id; ?>" name="id"><?php echo "S".$supplier_id; ?></td>
								</tr>
								<tr>
									<td>Name:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $name; ?>" name="name"><?php echo $name; ?></td>
								</tr>
								<tr>
									<td>Phone Number:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $phone; ?>" name="phone"><?php echo $phone; ?></td>
								</tr>
								<tr>
									<td>Company:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $compn; ?>" name="compn"><?php echo $compn; ?></td>
								</tr>
								<tr>
									<td class="print_td">Company P/H:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $cphone; ?>" name="cphone"><?php echo $cphone; ?></td>
								</tr>
								<tr>
									<td>Email:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $email; ?>" name="email"><?php echo $email; ?></td>
								</tr>
								<tr>
									<td>Address:</td>
									<td style="width:5px;"><input type="hidden" value="<?php echo $address; ?>" name="address"><?php echo $address; ?></td>
								</tr>
							</table>
							</div>
							<div class="printsize" style="float:left;margin:0 0 0 20%;">
							<label><u>Purchase Order Detail</u></label>
							<table class="adjusttd">
								<tr>
									<td>Delivery Order No:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $doid; ?>" name="doid"><?php echo $doid; ?></td>
								</tr>
								<tr>
									<td>Purchase Order No:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $poid; ?>" name="poid"><?php echo "P".$poid; ?></td>
								</tr>
								<tr>
									<td>Delivery Order Date:&nbsp;</td>
									<td><input type="hidden" value="<?php echo $date; ?>" name="date"><?php echo date("d/m/Y",strtotime($date)); ?></td>
								</tr>
							</table>
							</div>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table id="datatable" class="table table-striped table-hover" style="border-bottom:1px solid #ddd;">
									<thead>
										<tr>
											<th>No</th>
											<th>Item</th>
											<th>Quantity</th>
											<th>Received Quantity</th>
										</tr>
									</thead>
									<tbody>
									<?php
										$no=1;
										$subtotal=0;
										$query4=mysql_query("SELECT * FROM delivery_order_details WHERE DOID='".$_GET['doid']."' AND item_name!=''");
										while($row2=mysql_fetch_assoc($query4)){
											//$id=$row2['ID'];
									?>
									<tr>
										<td><?php echo $no++;?></td>
										<td><?php echo $row2['item_name'];?></td>
										<td><?php echo $row2['quantity'];?></td>
										<td><?php echo $row2['receive_quantity'];?></td>
									</tr>
									<?php 
										}
									?>
									</tbody>
								</table>
								</br>
								<a class="print btn btn-default" onclick="print(document)"><span class="glyphicon glyphicon-print"></span> Print</a>
								<!--<a href="invoice.php?id=<?php echo $_GET['poid'];?>" name="generate" class="btn btn-success">Generate Invoice</a>-->
									<input class="btn btn-default" type="button" onclick="history.back();" value="Back">
							</div>
						</div>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
<?php
	include 'interface/footer.php';
	}
?>
