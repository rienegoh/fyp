<?php
	include('../assets/config/config.php');
	error_reporting(0);
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
	<script>
		/*$(document).ready(function () {
			$('#datatable').dataTable({
				"language": {
					"decimal": ",",
					"thousands": ".",
					"lengthMenu": "Show _MENU_ supplier per page",
					"zeroRecords": "Nothing found",
					"info": "",
					"infoEmpty": "No records available",
					"infoFiltered": "(filtered from _MAX_ total records)"
				},
				"ordering": false,
				"bFilter":false,
				"paging":   false,
				//"order": [[ 0, "asc" ]],
				"bLengthChange":false,
				//"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollCollapse": true,
				"autoWidth": false,
				"sScrollX": "100%",
				"sScrollX": "visible: false"
			});							
		});
		jQuery('#dataTable').wrap('<div style="overflow:auto;" />');*/
	</script>

	
	<div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
					<h1 class="page-head-line">Customer Invoice</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-md-12">	
                    <div class="row">
					<form method="post" action="#">
					<div class="panel panel-default">
						<div class="panel-heading">
							Details
						</div>
						<?php
							if(isset($_GET['doid'])){
								$ID=$_GET['doid'];
								$query2=mysql_query("SELECT * FROM customer_delivery_order INNER JOIN customer ON customer_delivery_order.customer_id=customer.ID WHERE DOID='$ID'")or die(mysql_error());
								$countq2=mysql_num_rows($query2);
								if($countq2=1){
								while($row = mysql_fetch_assoc($query2)){
									$supplier_id=$row['customer_id'];
									$poid=$row['po_id'];
									$customer_fname=$row['firstName'];
									$customer_lname=$row['lastName'];
									$doid=$row['DOID'];
									$phone=$row['phone'];
									$address=$row['address'];
									$email=$row['email'];
									$date=$row['date'];
								}
								}else{
									$supplier_id='';
									$poid='';
									$doid='';
									$phone='';
									$address='';
									$email='';
									$date='';
								}
							}	
						?>
						<div class="panel-body">
							<div class="table-responsive">
								<table id="datatable" class="table table-hover cell-border table-bordered" style="border-bottom:1px solid #ddd;">
									<tbody>
										<tr>
											<td width="50%">Delivery ID</td>
											<td>CD<?php echo $doid; ?><input type="hidden" value="<?php echo $doid; ?>" name="doid"></td>
										</tr>
										<tr>
											<td width="50%">Invoice ID</td>
											<td>CI_<?php echo $poid; ?><input type="hidden" value="<?php echo $poid; ?>" name="poid"></td>
										</tr>
										<tr>
											<td width="50%">Delivery Date</td>
											<td><?php echo date("d-m-Y",strtotime($date)); ?><input type="hidden" value="<?php echo $date; ?>" name="date"></td>
										</tr>
										<tr>
											<td width="50%">Customer Name</td>
											<td><?php echo $customer_fname." ".$customer_lname; ?></td>
										</tr>
										<input type="hidden" value="<?php echo $date; ?>" name="po_date">
									</tbody>
								</table>
							</div>
							<div class="table-responsive">
								<table id="datatable" class="table table-striped table-hover" style="border-bottom:1px solid #ddd;">
									<thead>
										<tr>
											<th>No</th>
											<th>Item</th>
											<th>Quantity</th>
											<th>Price(RM)</th>
											<th>Sub Total(RM)</th>
										</tr>
									</thead>
									<tbody>
									<?php
										$no=1;
										$subtotal=0;
										$total=0;
										$subtotal=0;
										$query4=mysql_query("SELECT 
															customer_delivery_order_details.item_name,
															customer_delivery_order_details.sent_quantity,
															stock.name,
															stock.sellingPrice
															FROM `customer_delivery_order_details` INNER JOIN
															stock ON customer_delivery_order_details.item_name=stock.name
										WHERE DOID='".$_GET['doid']."'");
										while($row2=mysql_fetch_assoc($query4)){
											
											$subtotal=$row2['sent_quantity']*$row2['sellingPrice'];
									?>
									<tr>
										<td><?php echo $no++;?></td>
										<td><?php echo $row2['item_name'];?></td>
										<td><?php echo $row2['sent_quantity'];?></td>
										<td><?php echo number_format($row2['sellingPrice'],2);?></td>
										<td><?php echo number_format($subtotal,2);?></td>
									</tr>
									<?php 
									$total+=$subtotal;
										}
									?>
									<tr>
										<td colspan="4" style="text-align:right;">Total:</td>
										<td><?php echo number_format($total,2) ?></td>
									</tr>
									</tbody>
								</table>
								</br>
								<a class="print btn btn-default" onclick="print(document)"><span class="glyphicon glyphicon-print"></span> Print</a>
								<!--<a href="invoice.php?id=<?php echo $_GET['poid'];?>" name="generate" class="btn btn-success">Generate Invoice</a>-->
									<input class="btn btn-default" type="button" onclick="history.back();" value="Back">
							</div>
						</div>
					</div>
					</form>
					</div>
				</div>
			</div>
		</div>
<?php
	include 'interface/footer.php';
	}
?>
