<?php
	include('../assets/config/config.php');
	
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
?>
        <div id="page-wrapper"">
            <div id="page-inner" style="min-height:550;">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">My Profile</h1>
                    </div>
                </div>
				<?php
				if($_GET['id']){
					$id=filter_var($_GET['id'],FILTER_SANITIZE_NUMBER_INT);
					$q = mysql_query("SELECT *FROM staff WHERE ID=$id");
					while($row=mysql_fetch_assoc($q)){
						$id=$row['ID'];
						$fname=$row['firstName'];
						$lname=$row['lastName'];
						$email=$row['email'];
						$ph=$row['phone'];
						$gender=$row['gender'];
						$address=$row['address'];
						//$cname=$row['cname'];
						//$cphone=$row['cphone'];
						$ic=$row['ic'];
						$dob=$row['dob'];
						$username=$row['username'];
						$password=$row['password'];
						//$type=$row['type'];
					}
				}
				?>
                <div class="row">
                    <div class="col-md-6">
						<div class="row">
							<div class="panel panel-default">
								<div class="panel-heading">
									My Detail
								</div>
								<div class="panel-body">
									<dl class="dl-horizontal">
										<dt>Name:</dt>
										<dd>&nbsp;<?php echo $fname." ".$lname; ?></dd>
										<dt>Username:</dt>
										<dd>&nbsp;<?php echo $username; ?></dd>
										<dt>Password:</dt>
										<dd>&nbsp;<?php echo md5($password); ?></dd>
										<dt>Email:</dt>
										<dd>&nbsp;<?php echo $email; ?></dd>
										<dt>Phone number:</dt>
										<dd>&nbsp;<?php echo $ph; ?></dd>
										<dt>I/C:</dt>
										<dd>&nbsp;<?php echo $ic; ?></dd>
										<dt>Date of birth:</dt>
										<dd>&nbsp;<?php echo $dob; ?></dd>
										<dt>Gender:</dt>
										<dd>&nbsp;<?php echo $gender; ?></dd>
										<dt>Address:</dt>
										<dd style="width:50%;">
										<?php
											//echo $address;
											$addr=explode("|",$address,2);
											echo $addr[0].", ".$addr[1];
										?>
										</dd>
										<!--<dt>Company name:</dt>
										<dd>&nbsp;<?php //echo $cname; ?></dd>
										<dt>Company phone number:</dt>
										<dd>&nbsp;<?php //echo $cphone; ?></dd>-->
									</dl>
									<a href="editadmin.php?edit=<?php echo $id;?>" class="btn btn-primary">
										<i class="glyphicon glyphicon-edit">&nbsp;</i>
										Edit
									</a>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
<?php
	include 'interface/footer.php';
	}
?>