<?php
	include('../assets/config/config.php');
	error_reporting(0);
	if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')) {
		header ("Location: ../customer/index.php");
	}else{
		include 'interface/head.php';
	
?>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
						<h1 class="page-head-line">Add Category</h1>
                    </div>
                </div>
				<?php	
					if(isset($_POST['add'])){
						//$c_iso=substr($_POST['category_name'],0,1);
						//$c_iso=preg_match_all('/[A-Z][^A-Z]*/',$_POST['category_name'],$results);
						$num = $_POST['num'];
						for($i=0;$i<=$num;$i++){
						$csf=$_POST['category_shortform'][$i];
						$cname=$_POST['category_name'][$i];
						$des=$_POST['description'][$i];
						$image=$_FILES["images"]["name"][$i];
						$imageData=$_FILES["images"]["tmp_name"][$i];
							if(empty($cname)||empty($csf))
							{
								$emp=true;
							}else{
								if($image){
									if(($_FILES['images']['type'][$i]=='image/gif')
									|| ($_FILES['images']['type'][$i]=='image/jpeg')
									|| ($_FILES['images']['type'][$i]=='image/pjpeg')
									|| ($_FILES['images']['size'][$i]<200000)){
									if(move_uploaded_file($_FILES['images']['tmp_name'][$i],"../assets/images/".$_FILES['images']['name'][$i])){
									$mysql=mysql_query("INSERT INTO category VALUES ('', '$csf', '$cname', '$image', '$imageData','$des')");
									}
									}else{
										$image_type=true;
									}
								}else{
									$mysql=mysql_query("INSERT INTO category VALUES ('', '$csf', '$cname', '$image', '$imageData','$des')");
								}
							}
						}
						if($mysql){
							echo "<div class='alert alert-info'>".
							"Successfully".
							"</div>";
						}else if($emp){
							echo "<div class='alert alert-danger'>".
								"Please fill out all the fields".
								"</div>";
						}else if($image_type){
							echo "<div class='alert alert-danger'>".
								"Image type must be GIF,JPG or PJPEG.".
								"</div>";
						}else{
							echo"<div class='alert alert-danger'>".
							"Something Wrong".
							"</div>";
						}
						/*echo '<pre>';
						print_r($_POST);
						echo '</pre>';*/
					}
				?>
				<form method="post" action="add_category.php" enctype="multipart/form-data">
				<div class="form-inline" style="margin:0 0 1% 2.5%;">
					<label>Add</label>
					<input class="form-control" style="width:6%;" name="num" type="number" min="0" value="<?php echo $_POST['num']; ?>">
					<label>row</label>
					<input type="submit" class="btn btn-default" name="addrecord" value="Go">
				</div>
                <div class="row">
                    <div class="col-md-12">
						<div class="row">
							<div class="panel panel-default">
							<?php
								if($_POST['submit']='addrecord'){
									$num = $_POST['num'];
									for($i=0;$i<=$num;$i++){
							?>
								<div class="panel-heading">
									New Category #<?php echo $i; ?>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<div class="form-group">
											<label>Category Short Form:</label>
											<input class="form-control" name="category_shortform[]" id="category_shortform" placeholder="E.g. T" type="text" >
										</div>
										<div class="form-group">
											<label>Category Name:</label>
											<input class="form-control" name="category_name[]" id="category_name" placeholder="E.g. Test" type="text" >
										</div>
										<div class="form-group">
											<label>Image:</label>
											<input type="file" name="images[]">
										</div>
										<div class="form-group">
											<label>Description:</label>
											<textarea class="form-control" name="description[]" id="description"></textarea>
										</div>
									</div>
								</div>
								<?php
										}
									}
								?>
								<input type="submit" class="btn btn-info" style="margin:-0.5% 0 1% 1%;" id="add" name="add" value="Add Category">
							</div>
						</div>
					</div>
                </div>
				</form>
            </div>
        </div>
<?php
	include 'interface/footer.php';
	}
?>