-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 29, 2016 at 06:29 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `ID` int(5) unsigned zerofill NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(12) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `ic` varchar(14) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `address` varchar(250) NOT NULL,
  `type` varchar(13) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`ID`, `username`, `password`, `firstName`, `lastName`, `email`, `dob`, `ic`, `phone`, `gender`, `address`, `type`) VALUES
(00001, 'admin', '999999', 'Wong', 'Jun Wei', 'jw@hotmail.com', '1995-02-23', '950223-01-6666', '010-7669997', 'Male', '7,jalan tun abdula, taman abdula|80100 Johor Bahru', 'Administrator'),
(00002, 'liangyuan', '8787987', 'Cho', 'Liang Guan', 'ly@hotmail.com', '1995-01-01', '950101-01-6587', '010-7707077', 'Male', '87,Jalan Cho Liang Guan|80220, Pahang', 'Administrator'),
(00003, 'riene', '9487487', 'Riene Goh', 'Sin Ling', 'riene@hotmail.com', '1995-02-28', '950228-01-6652', '010-7705231', 'Female', '85,jalan riene goh|80230, Johor Bahru', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `ID` int(11) NOT NULL,
  `total` double NOT NULL,
  `date` date NOT NULL,
  `customer_name` varchar(50) NOT NULL,
  `stock_id` int(5) unsigned zerofill NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cart_details`
--

CREATE TABLE IF NOT EXISTS `cart_details` (
  `ID` int(11) NOT NULL,
  `customer_id` varchar(50) NOT NULL,
  `cart_id` int(5) unsigned zerofill NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  `subtotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `CID` int(11) NOT NULL,
  `category_shortform` varchar(3) CHARACTER SET utf8 NOT NULL,
  `categoryName` text NOT NULL,
  `image` text NOT NULL,
  `image_data` blob NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`CID`, `category_shortform`, `categoryName`, `image`, `image_data`, `description`) VALUES
(1, 'HG', 'HOME & GARDEN', 'catban-home&garden.jpg', 0x443a78616d7070096d70706870363736432e746d70, 'Make your house a home with our inspirational home & garden selections and decorative ideas. Be thrilled with our wide variety and save even more when you shop with us!\r\n'),
(2, 'B', 'BATHROOM', 'catban-bathroom (1).jpg', 0x443a78616d7070096d70706870463234322e746d70, 'We join you in the creation of your most personal and private place in your homes â€“ the kitchen and bathroom. With products ranging from big items like cooking hoods, gas hobs, kitchen sinks and bathtubs to smaller accessories like faucets and towel hooks, we bring you a world of diversity when it comes to creating your own space. Let your creativity rules!\r\n'),
(3, 'PT', 'POWER TOOLS', 'catban-powertools (1).jpg', 0x443a78616d7070096d70706870353044332e746d70, 'We provide a wide range of power tools and machinery to help increase speed and accuracy when it comes to cutting, drilling, shaping, sanding, grinding and polishing. Make your construction work easy today with our finest range of power tools and machinery. Explore it now!\r\n'),
(4, 'HD', 'HARDWARE', 'catban-hardware.jpg', 0x443a78616d7070096d707068703134452e746d70, 'We have all the hardwares and tools to assist you with project. Whether you are a professional looking for diversified and high quality brands or a skilled end-user looking for the finest tools, we are always here to assist you!\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=240 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D''IVOIRE', 'Cote D''Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF', 'Korea, Democratic People''s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE''S DEMOCRATIC REPUBLIC', 'Lao People''s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `ID` int(5) unsigned zerofill NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `country_iso` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `gender` text NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `cname` text NOT NULL,
  `cphone` text NOT NULL,
  `username` text NOT NULL,
  `password` varchar(12) NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`ID`, `firstName`, `lastName`, `email`, `country_iso`, `phone`, `gender`, `address1`, `address2`, `cname`, `cphone`, `username`, `password`, `type`) VALUES
(00001, 'Lim', 'Wei Hao', 'lwh@hotmail.com', 'MY', '010-8787987', 'Male', '78,Jalan Dator Onn', '80200,Johor Bahru', 'Heng Heng', '07-8787587', 'weihao', '123456', 'Customer'),
(00002, 'Lee', 'Wei Jie', 'wj@hotmail.com', 'MY', '010-7879487', 'Male', '15,Jalan Imian Emas,...', '80200,Johor Bahru', 'Heng Heng', '07-8787587', 'weijie', '123456', 'Customer'),
(00003, 'Cho', 'Liang Guan', 'mac_cho@hotmail.com', 'MY', '016-8446549', 'Male', '87,Jalan Cho', '80200, Johor Bahru', '-', '-', 'clg', '999999', 'Customer');

-- --------------------------------------------------------

--
-- Table structure for table `customer_delivery_order`
--

CREATE TABLE IF NOT EXISTS `customer_delivery_order` (
  `ID` int(11) NOT NULL,
  `DOID` int(5) unsigned zerofill NOT NULL,
  `po_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `po_id` int(5) unsigned zerofill NOT NULL,
  `customer_id` int(5) unsigned zerofill NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_delivery_order`
--

INSERT INTO `customer_delivery_order` (`ID`, `DOID`, `po_date`, `date`, `po_id`, `customer_id`) VALUES
(1, 00001, '2016-11-29 05:15:40', '2016-11-29 05:16:14', 00001, 00001);

-- --------------------------------------------------------

--
-- Table structure for table `customer_delivery_order_details`
--

CREATE TABLE IF NOT EXISTS `customer_delivery_order_details` (
  `ID` int(11) NOT NULL,
  `POID` int(5) unsigned zerofill NOT NULL,
  `DOID` int(5) unsigned zerofill NOT NULL,
  `item_name` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `sent_quantity` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_delivery_order_details`
--

INSERT INTO `customer_delivery_order_details` (`ID`, `POID`, `DOID`, `item_name`, `quantity`, `sent_quantity`) VALUES
(1, 00001, 00001, 'HEAVY DUTY FPT-630 FOLDABLE PLATFORM HAND TRUCK', 0, 1),
(2, 00001, 00001, 'SKIL 8715 WET & DRY VACUUM CLEANER 15L', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `customer_purchase_order`
--

CREATE TABLE IF NOT EXISTS `customer_purchase_order` (
  `ID` int(11) NOT NULL,
  `customer_name` text NOT NULL,
  `POID` int(5) unsigned zerofill NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `customer_id` int(5) unsigned zerofill NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_purchase_order`
--

INSERT INTO `customer_purchase_order` (`ID`, `customer_name`, `POID`, `date`, `customer_id`, `status`) VALUES
(1, 'weihao', 00001, '2016-11-29 05:15:40', 00001, 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `customer_purchase_order_details`
--

CREATE TABLE IF NOT EXISTS `customer_purchase_order_details` (
  `ID` int(11) NOT NULL,
  `POID` int(5) unsigned zerofill NOT NULL,
  `item_name` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `original_quantity` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  `total_price` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_purchase_order_details`
--

INSERT INTO `customer_purchase_order_details` (`ID`, `POID`, `item_name`, `quantity`, `original_quantity`, `unit_price`, `total_price`) VALUES
(1, 00001, 'HEAVY DUTY FPT-630 FOLDABLE PLATFORM HAND TRUCK', 0, 1, 300, 300),
(2, 00001, 'SKIL 8715 WET & DRY VACUUM CLEANER 15L', 0, 3, 400, 1200);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_order`
--

CREATE TABLE IF NOT EXISTS `delivery_order` (
  `ID` int(11) NOT NULL,
  `DOID` text NOT NULL,
  `po_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `po_id` int(5) unsigned zerofill NOT NULL,
  `supplier_id` int(5) unsigned zerofill NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_order`
--

INSERT INTO `delivery_order` (`ID`, `DOID`, `po_date`, `date`, `po_id`, `supplier_id`) VALUES
(1, '00055', '2016-11-29 05:20:03', '2016-11-29 05:20:22', 00001, 00002),
(2, '00056', '2016-11-29 05:20:03', '2016-11-29 05:21:45', 00001, 00002);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_order_details`
--

CREATE TABLE IF NOT EXISTS `delivery_order_details` (
  `ID` int(11) NOT NULL,
  `POID` int(5) unsigned zerofill NOT NULL,
  `DOID` text NOT NULL,
  `item_name` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `receive_quantity` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_order_details`
--

INSERT INTO `delivery_order_details` (`ID`, `POID`, `DOID`, `item_name`, `quantity`, `receive_quantity`) VALUES
(1, 00001, '00055', 'BOB 727-F ELEGANT DESIGN WALL TYPE CHROME BIB TAP 1/', 12, 0),
(2, 00001, '00055', 'BOB 727-X ELEGANT DESIGN WALL TYPE CHROME BIB TAP 1/2', 121, 1),
(3, 00001, '00056', 'BOB 727-F ELEGANT DESIGN WALL TYPE CHROME BIB TAP 1/', 6, 6),
(4, 00001, '00056', 'BOB 727-X ELEGANT DESIGN WALL TYPE CHROME BIB TAP 1/2', 114, 7);

-- --------------------------------------------------------

--
-- Table structure for table `leave`
--

CREATE TABLE IF NOT EXISTS `leave` (
  `leaveID` int(5) unsigned zerofill NOT NULL,
  `staffName` text NOT NULL,
  `staffID` int(5) unsigned zerofill NOT NULL,
  `annual` int(255) NOT NULL,
  `medical` int(255) NOT NULL,
  `withoutPay` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `leavedetails`
--

CREATE TABLE IF NOT EXISTS `leavedetails` (
  `ID` int(5) unsigned zerofill NOT NULL,
  `name` varchar(255) NOT NULL,
  `ic` int(255) NOT NULL,
  `fromDate` date NOT NULL,
  `toDate` date NOT NULL,
  `totalDay` int(255) NOT NULL,
  `leaveType` varchar(255) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `leaveID` int(5) unsigned zerofill NOT NULL,
  `status` varchar(255) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE IF NOT EXISTS `purchase_order` (
  `ID` int(11) NOT NULL,
  `POID` int(5) unsigned zerofill NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `supplier_id` int(5) unsigned zerofill NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`ID`, `POID`, `date`, `supplier_id`) VALUES
(1, 00001, '2016-11-29 05:20:03', 00002);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_details`
--

CREATE TABLE IF NOT EXISTS `purchase_order_details` (
  `ID` int(11) NOT NULL,
  `POID` int(5) unsigned zerofill NOT NULL,
  `item_name` text NOT NULL,
  `original_quantity` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  `total_price` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order_details`
--

INSERT INTO `purchase_order_details` (`ID`, `POID`, `item_name`, `original_quantity`, `quantity`, `unit_price`, `total_price`) VALUES
(1, 00001, 'BOB 727-F ELEGANT DESIGN WALL TYPE CHROME BIB TAP 1/', 12, 6, 20, 240),
(2, 00001, 'BOB 727-X ELEGANT DESIGN WALL TYPE CHROME BIB TAP 1/2', 122, 114, 20, 2440);

-- --------------------------------------------------------

--
-- Table structure for table `quantitytype`
--

CREATE TABLE IF NOT EXISTS `quantitytype` (
  `ID` int(11) NOT NULL,
  `quantity_type` text NOT NULL,
  `category` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quantitytype`
--

INSERT INTO `quantitytype` (`ID`, `quantity_type`, `category`, `description`) VALUES
(1, 'units', '', 'units'),
(2, 'rolls', '', 'rolls'),
(3, 'pcs', '', 'Port Congestion Surcharge'),
(4, 'boxes', '', 'boxes');

-- --------------------------------------------------------

--
-- Table structure for table `shipping`
--

CREATE TABLE IF NOT EXISTS `shipping` (
  `ID` int(5) unsigned zerofill NOT NULL,
  `date` date NOT NULL,
  `status` text NOT NULL,
  `transportID` int(5) unsigned zerofill NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shipping`
--

INSERT INTO `shipping` (`ID`, `date`, `status`, `transportID`) VALUES
(00001, '2016-11-29', 'active', 00001);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_details`
--

CREATE TABLE IF NOT EXISTS `shipping_details` (
  `ID` int(5) unsigned zerofill NOT NULL,
  `shippingID` int(5) unsigned zerofill NOT NULL,
  `DOID` int(5) unsigned zerofill NOT NULL,
  `status` text NOT NULL,
  `address` varchar(255) NOT NULL,
  `issuePerson` text NOT NULL,
  `issueDateTime` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shipping_details`
--

INSERT INTO `shipping_details` (`ID`, `shippingID`, `DOID`, `status`, `address`, `issuePerson`, `issueDateTime`) VALUES
(00001, 00001, 00001, 'active', '78,Jalan Dator Onn,80200,Johor Bahru', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `ID` int(5) unsigned zerofill NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` int(12) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `ic` varchar(14) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `address` varchar(250) NOT NULL,
  `hire_date` date NOT NULL,
  `basic_salary` int(11) NOT NULL,
  `type` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`ID`, `username`, `password`, `firstName`, `lastName`, `email`, `dob`, `ic`, `phone`, `gender`, `address`, `hire_date`, `basic_salary`, `type`) VALUES
(00001, 'staff', 999999, 'Wong', 'Jun Wei', 'tdjunwei@hotmail.com', '1995-03-04', '950304-01-6666', '010-7382878', 'Male', '88,Jalan Tun Mutiara, Taman Mutiara|80120 Johor Bahru', '2016-07-13', 8500, 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `ID` int(5) unsigned zerofill NOT NULL,
  `category_prefix` varchar(2) NOT NULL DEFAULT 'M',
  `name` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `quantity_type` text NOT NULL,
  `image` text NOT NULL,
  `image_data` blob NOT NULL,
  `companyPrice` double NOT NULL,
  `sellingPrice` double NOT NULL,
  `date` date NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`ID`, `category_prefix`, `name`, `quantity`, `quantity_type`, `image`, `image_data`, `companyPrice`, `sellingPrice`, `date`, `description`) VALUES
(00001, 'HG', 'SKIL 8715 WET & DRY VACUUM CLEANER 15L', 60, 'units', 'Skil-8715-Wet-Dry-700x700.jpg', 0x443a78616d7070096d70706870324237362e746d70, 200, 400, '2016-11-29', 'Powerful 1500W motor.\r\nThis appliance is intended for the collection and vacuuming of non-flammable liquids as well as dry materials.\r\nThis appliance is intended for indoor use only and suitable usage as required in hotels, schools, hospitals, factories, '),
(00002, 'B', 'BOB 727-F ELEGANT DESIGN WALL TYPE CHROME BIB TAP 1/', 72, 'units', 'bob-727fa-700x700.jpg', 0x443a78616d7070096d70706870443443342e746d70, 20, 30, '2016-11-29', 'Elegant design\r\nBrass ceramic valve\r\nCountry of Origin: China'),
(00003, 'B', 'BOB 727-X ELEGANT DESIGN WALL TYPE CHROME BIB TAP 1/2', 86, 'units', 'bob-727xa-700x700.jpg', 0x443a78616d7070096d70706870333244442e746d70, 20, 30, '2016-11-29', 'Elegant design\r\nBrass ceramic valve\r\nCountry of Origin: China'),
(00004, 'PT', 'BOSCH GDM 121 MARBLE SAW PROFESSIONAL 1250W', 78, 'pcs', 'bosch gdm 121-700x700.jpg', 0x443a78616d7070096d70706870323444412e746d70, 200, 300, '2016-11-29', 'Extremely with powerful motor: 1,300 watts.\r\nGood dust insulation to keep the powerful motor for reliable life time.\r\nLight weight thanks to ergonomic design.\r\nDurable ball-bearing construction for longer lifetime\r\nErgonomic Handle for continuous work, ea'),
(00005, 'PT', 'BOSCH GEX 125-1 A RANDOM ORBIT SANDER PROFESSIONAL 250W', 322, 'pcs', 'gex 125-1 a-700x700.jpg', 0x443a78616d7070096d70706870313945332e746d70, 200, 300, '2016-11-29', 'Extremely with powerful motor: 250 watts.\r\nFatigue-free sanding in any position\r\nCompact, ergonomic design with three softgrip surfaces for variable grip positions and outstanding convenience.\r\nBosch Micro-filter system: perfect, efficient dust extraction'),
(00006, 'PT', 'BOSCH AKE 40-19 S CHAINSAW 1900W', 78, 'pcs', 'AKE 40-19S-700x700.jpg', 0x443a78616d7070096d70706870453841372e746d70, 500, 600, '2016-11-29', 'Extremely with powerful motor: 1900 watts with a chain speed of 12 m/s for outstanding cutting performance.\r\nEasy to use: SDS system for tool-free changing and tensioning of the chain.\r\nExtremely robust: top quality for frequent use and long life.\r\nOptimu'),
(00007, 'HD', 'HEAVY DUTY FPT-630 FOLDABLE PLATFORM HAND TRUCK', 77, 'pcs', 'truck trolley-700x700.jpg', 0x443a78616d7070096d70706870394637382e746d70, 150, 300, '2016-11-29', 'Max. Load (ML) Capacity: 150kg\r\nLength: 670mm\r\nWidth: 410mm\r\nCastor Wheel Size: 4" \r\nColour: Black\r\n'),
(00008, 'HD', 'LSTM HEAVY DUTY TROLLEY', 78, 'pcs', 'LSTM-HD10A-700x700.jpg', 0x443a78616d7070096d70706870413638422e746d70, 100, 200, '2016-11-29', 'Width: 430mm\r\nDepth: 355mm\r\nHeight: 1325mm\r\nWheels size: 10"'),
(00009, 'HD', 'TAHAN PVC PLATFORM TWO TIER HAND TRUCK', 78, 'pcs', 'tahan-thp512-700x700.jpg', 0x443a78616d7070096d70706870383045332e746d70, 300, 500, '2016-11-29', 'Tahan PVC Platform Two Tier Hand Truck 900MM (L) x 600MM (W) 300KG (Max. Load) with Protective Mesh\r\n \r\n'),
(00010, 'B', 'GROHE BAUCURVE 31226000 SINK TAP 1/2', 78, 'pcs', '31226000-1-700x700.jpg', 0x443a78616d7070096d70706870413342462e746d70, 300, 500, '2016-11-29', 'GROHE BauCurve Sink tap 1/2" wall mounted\r\nMetal lever\r\nCeramic headpart\r\nSwivelling tubular spout\r\nMousseur\r\nChrome finish\r\n'),
(00011, 'HD', 'EVA SDS-PLUS MASONRY DRILL BIT HAMMER 10.0MM (D) X 150/210MM (L)', 0, 'units', 'eva-sds-700x700.jpg', 0x443a78616d7070096d70706870443030452e746d70, 11, 30, '2016-11-29', 'Diameter: 10.0mm\r\nLength: 150mm\r\nWorking length: 210mm\r\nDrilling mode: Hammer drilling, drilling\r\nBase material: Masonry');

-- --------------------------------------------------------

--
-- Table structure for table `stock_adjustment`
--

CREATE TABLE IF NOT EXISTS `stock_adjustment` (
  `ID` int(5) unsigned zerofill NOT NULL,
  `item` text NOT NULL,
  `reason` text NOT NULL,
  `date` datetime NOT NULL,
  `lastModify` datetime NOT NULL,
  `quantity` int(11) NOT NULL,
  `value` double NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_adjustment`
--

INSERT INTO `stock_adjustment` (`ID`, `item`, `reason`, `date`, `lastModify`, `quantity`, `value`, `total`) VALUES
(00001, 'SKIL 8715 WET & DRY VACUUM CLEANER 15L', 'missing', '2016-11-29 13:19:06', '2016-11-29 13:19:06', -1, 200, 0);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `ID` int(5) unsigned zerofill NOT NULL,
  `company_name` text NOT NULL,
  `country_iso` text NOT NULL,
  `company_phone` text NOT NULL,
  `person_in_charge` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(250) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`ID`, `company_name`, `country_iso`, `company_phone`, `person_in_charge`, `phone`, `email`, `address`) VALUES
(00002, 'Eniee Hardware Sdn.Bhd', 'MY', '07-9975135', 'Lam Teck', '012-8833910', 'lamteck22@hotmail.com', 'Lot 315, Jalan Pknk 3 Kawasan, Perindustrian Sungai Petani|08000, Kedah'),
(00001, 'Kee Sheng Harware Enterprise', 'MY', '07-7549135', 'Kee Sheng', '018-6739490', 'kee_sheng@hotmail.com', 'Lot 219, Km 13 Bertam Ulu|76450, Melaka'),
(00004, 'Soon Ming Electrical Services', 'AW', '07-6453454', 'Soon Ming', '012-1231231', 'soonming@hotmail.com', 'No.7, Jalan Tembaga Sd5/2e|52200, k.l.|');

-- --------------------------------------------------------

--
-- Table structure for table `transport`
--

CREATE TABLE IF NOT EXISTS `transport` (
  `ID` int(5) unsigned zerofill NOT NULL,
  `carName` varchar(65) NOT NULL,
  `carNumber` varchar(7) NOT NULL,
  `Status` varchar(65) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transport`
--

INSERT INTO `transport` (`ID`, `carName`, `carNumber`, `Status`) VALUES
(00001, 'Lori', 'ABC 111', 'No Duty'),
(00002, 'VIVA', 'ABB 110', 'No Duty');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `cart_details`
--
ALTER TABLE `cart_details`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`CID`,`category_shortform`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `customer_delivery_order`
--
ALTER TABLE `customer_delivery_order`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `customer_delivery_order_details`
--
ALTER TABLE `customer_delivery_order_details`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `customer_purchase_order`
--
ALTER TABLE `customer_purchase_order`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `customer_purchase_order_details`
--
ALTER TABLE `customer_purchase_order_details`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `delivery_order`
--
ALTER TABLE `delivery_order`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `delivery_order_details`
--
ALTER TABLE `delivery_order_details`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `leave`
--
ALTER TABLE `leave`
  ADD PRIMARY KEY (`leaveID`);

--
-- Indexes for table `leavedetails`
--
ALTER TABLE `leavedetails`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `purchase_order_details`
--
ALTER TABLE `purchase_order_details`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `quantitytype`
--
ALTER TABLE `quantitytype`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `shipping_details`
--
ALTER TABLE `shipping_details`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `stock_adjustment`
--
ALTER TABLE `stock_adjustment`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `transport`
--
ALTER TABLE `transport`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `ID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cart_details`
--
ALTER TABLE `cart_details`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `CID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `ID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `customer_delivery_order`
--
ALTER TABLE `customer_delivery_order`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customer_delivery_order_details`
--
ALTER TABLE `customer_delivery_order_details`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customer_purchase_order`
--
ALTER TABLE `customer_purchase_order`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customer_purchase_order_details`
--
ALTER TABLE `customer_purchase_order_details`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `delivery_order`
--
ALTER TABLE `delivery_order`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `delivery_order_details`
--
ALTER TABLE `delivery_order_details`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `leave`
--
ALTER TABLE `leave`
  MODIFY `leaveID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `leavedetails`
--
ALTER TABLE `leavedetails`
  MODIFY `ID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `purchase_order_details`
--
ALTER TABLE `purchase_order_details`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `quantitytype`
--
ALTER TABLE `quantitytype`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `shipping`
--
ALTER TABLE `shipping`
  MODIFY `ID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shipping_details`
--
ALTER TABLE `shipping_details`
  MODIFY `ID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `ID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `ID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `stock_adjustment`
--
ALTER TABLE `stock_adjustment`
  MODIFY `ID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `ID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `transport`
--
ALTER TABLE `transport`
  MODIFY `ID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
