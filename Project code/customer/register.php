<?php
	require('../assets/config/config.php');

	
if(isset($_POST['register'])){
		$fname=$_POST['fname'];
		$lname=$_POST['lname'];
		$email=$_POST['email'];
		$country=$_POST['select_country'];
		$phone=$_POST['phone'];
		$gender=$_POST['gender'];
		$address1=$_POST['address1'];
		$address2=$_POST['address2'];
		$cname=$_POST['cname'];
		$cphone=$_POST['cphone'];
		$username=$_POST['username'];
		$password=trim($_POST['password']);
		if(empty($fname )|| empty($lname) || empty($email) || empty($phone) || empty($gender) 
		|| empty($address1) || empty($address2) || empty($username) || empty($password))
		{
			echo"<script type=\"text/javascript\">".
				"alert('You did not fill out all fields!');".
				"window.location = 'register.php';".
				"</script>";
		}else{
			if($_POST['select_country']!='sc'){
				if((strlen($password) >= 6)&&(strlen($password) <= 12)){
					//check user exists or not!!!!!!!!!!
					$checkuser=mysql_query(
					"SELECT count(username) 
					FROM admin
					WHERE username='$username'
					UNION
					SELECT count(username) 
					FROM customer
					WHERE username='$username'");
					$calrow=mysql_num_rows($checkuser);
					if($calrow>1){
						echo "<script type=\"text/javascript\">".
							"alert('Username already exists!');".
							"window.location = 'register.php';".
							"</script>";
					}else{
						$mysql=mysql_query("INSERT INTO `customer`(`firstName`, `lastName`, `email`, `country_iso`, `phone`, `gender`, `address1`, `address2`, `cname`, `cphone`, `username`, `password`, `type`)
						VALUES ('$fname','$lname','$email','$country','$phone','$gender','$address1','$address2','$cname','$cphone','$username','$password','Customer')");
						if($mysql){
							echo"<script type=\"text/javascript\">".
								"alert('Register Successful!');".
								"window.location = '../login.php';".
								"</script>";
						}else{
							echo"<script type=\"text/javascript\">".
							"alert('Something Wrong!');".
							"window.location = 'register.php';".
							"</script>";
						}
					}
				}else{
					echo"<script type=\"text/javascript\">".
						"alert('Password must be at least 6 or 12 characters or number.');".
						"window.location = 'register.php';".
						"</script>";
				}
			}else{
				echo"<script type=\"text/javascript\">".
					"alert('Please select your country.');".
					"window.location = 'register.php';".
					"</script>";
			}
		}
	}
?>
<html lang="en">
<head>
    <title>Home | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">      

</head><!--/head-->

<body>
	<header id="header"><!--header-->

		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><img src="images/home/logo.png" alt="" height="100" width="200"/></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li class="active"><?php include ('userlogin.php'); ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php">Home</a></li>
								<li><a href="AllCategory.php">All Category</a></li> 
								<li><a href="contactUs.php">Contact Us</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-9 padding-right">
					<div class="features_items" align="center"><!--features_items-->
						<h2 class="title text-center">register</h2>
							<form method="POST" action="#">
								<table border="0" cellpadding="10">
									<tr>
										<td>First Name</td> 
										<td>:</td>
										<td><input type="text" class="form-control" name="fname"  ></td>
										<td>Last Name</td> 
										<td>:</td>
										<td><input type="text" class="form-control" name="lname"  ></td>
									</tr>
									<tr>
										<td>Username</td> 
										<td>:</td>
										<td><input type="text" class="form-control" name="username"  ></td>
									</tr>
									<tr>
										<td>Password:</td> 
										<td>:</td>
										<td><input type="password" class="form-control" name="password"></td>
										<td>Confirm Password:</td> 
										<td>:</td>
										<td><input type="password" class="form-control" name="confirm_password"></td>
									</tr>
									<tr>
										<td>DOB</td> 
										<td>:</td>
										<td><input type="date" class="form-control" name="dob"></td>
										<td>Email</td> 
										<td>:</td>
										<td><input type="email" class="form-control" name="email"></td>
									</tr>
									<tr>
										<td>Country</td> 
										<td>:</td>
										<td>
										<select class="form-control" name="select_country">
											<option selected="selected" value="sc">--Select Country--</option>
										<?php
											$q = mysql_query("SELECT * FROM country ORDER BY nicename ASC");
											while($row2=mysql_fetch_assoc($q)){
										?>
											<option value="<?php echo $row2['iso']; ?>"><?php echo $row2['nicename']." &rlm;"."(&lrm;+".$row2['phonecode'].")"; ?></option>
										<?php
											}
										?>
										</select>
										</td>
										<td>Phone</td> 
										<td>:</td>
										<td><input type="text" class="form-control" name="phone" maxlength="11"></td>
									</tr>
									<tr>
										<td>Gender</td> 
										<td>:</td>
										<td>
										<input type="radio" name="gender" value="Male" checked="">Male
										<input type="radio" name="gender" value="Female">Female
										</td>
									</tr>
									<tr>
										<td>Address 1</td> 
										<td>:</td>
										<td><input type="text" class="form-control" name="address1"></td>
									</tr>
									<tr>
										<td>Address 2</td> 
										<td>:</td>
										<td><input type="text" class="form-control" name="address2"></td>
									</tr>
									<tr>
										<td>Company Name</td> 
										<td>:</td>
										<td><input type="text" class="form-control" name="cname"></td>
									</tr>
									<tr>
										<td>Company Phone</td> 
										<td>:</td>
										<td><input type="text" class="form-control" name="cphone">
											<input type="text" name="type" value="C" hidden>
										</td>
									</tr>
								</table>
								<input type="submit" class="btn btn-default" name="register" id="register" value="Register">
								<input type="reset" class="btn btn-default" value="Reset">
							</form>
					</div><!--Product Items-->
	
				</div>
			</div>
		</div>
	</section>
	
	
	<footer id="footer"><!--Footer-->
		
		<div class="footer-widget">
			
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2016.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href=""></a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer--> 
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>