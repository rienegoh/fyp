<?php
	require('../assets/config/config.php');
	
	if(isset($_POST['checkout'])){
		$num = count($_POST['item_price']);
		for($i=0;$i<$num;$i++){
		$name=$_POST['item_name'][$i];
		$price=$_POST['item_price'][$i];
		$quantity=$_POST['item_quantity'][$i];
		$subtotal=$_POST['subtotal'][$i];
		
		$query5=mysql_query("SELECT * FROM customer_purchase_order");
		$countall=mysql_num_rows($query5);
		$poid=$countall+1;
		$padded = str_pad((string)$poid, 5, "0", STR_PAD_LEFT); //auto fill in zero
		
		$customer_po_details=mysql_query("INSERT INTO customer_purchase_order_details VALUES ('','$padded','$name','$quantity','$price','$subtotal')");
		}
		$customer_po=mysql_query("INSERT INTO customer_purchase_order VALUES ('','".$_SESSION['username']."','$padded',CURDATE(),'".$_SESSION['user_id']."','Pending')");
		/*echo '<pre>';
		print_r($_POST);
		echo '</pre>';*/
	}
	
	$action = isset($_GET['action']) ? $_GET['action'] : "";
	$id = isset($_GET['id']) ? $_GET['id'] : "";
	$name = isset($_GET['name']) ? $_GET['name'] : "";
	if($action=="remove"){
		$id = isset($_GET['id']) ? $_GET['id'] : "";
		$name = isset($_GET['name']) ? $_GET['name'] : "";
		foreach($_SESSION["cart_items"] as $keys => $values)  
        {  
            if($values["item_id"] == $_GET["id"])  
			{  
				unset($_SESSION["cart_items"][$keys]);  
				header('Location: cart.php?id=' . $id . '&name=' . $name);
            }  
        } 
	}
	if($action=="empty"){
		unset($_SESSION['cart_items']);
	}
?>
<html lang="en">
<head>
    <title>Cart | ZhuanYou's Hardware</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">      

</head><!--/head-->

<body>
	<header id="header"><!--header-->

		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><img src="images/home/logo.png" alt="" height="100" width="200"/></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li class="active"><?php include ('userlogin.php'); ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php">Home</a></li>
								<li><a href="AllCategory.php">All Category</a></li> 
								<li><a href="contactUs.php">Contact Us</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb" style="margin-bottom:10px;">
					<li><a href="index.php">Home</a></li>
					<li class="active">Shopping Cart</li>
					<a href="cart.php?action=empty" onclick="return confirm('Are you sure you?')" class="btn btn-default" style="float:right;">Empty Cart</a>
				</ol>
			</div>
			<?php
				if($action=="updated"){
					echo "<div class='alert alert-info'>".
						"<b>$name</b> update successfully".
						"</div>";
				}
			?>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="image">ID	</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td class="total">action</td>
						</tr>
					</thead>
	<tbody>
	<?php
		//Array sorting
		function cmp($a, $b) {
			if ($a == $b) {
				return 0;
			}
			return ($a < $b) ? -1 : 1;
		}
		
		if(!empty($_SESSION["cart_items"]))  
        {
            $totalprice = 0; 
			$num=1; 
			uasort($_SESSION['cart_items'], 'cmp');//sorting
			
			/*echo "<pre>";
			print_r($_SESSION['cart_items']);
			//echo var_dump($_SESSION['cart_items']);
			//print_r(array_keys($_SESSION['cart_items']));
			echo "</pre>";	*/
			
            foreach($_SESSION["cart_items"] as $keys => $values)  
            {
			$subtotal=$values['item_price']*$values['quantity']; 
			$totalprice+=$subtotal;	
	?>
	
<form method="POST" action="update_quantity.php?action=update&id=<?php echo $values["item_id"]; ?>&name=<?php echo $values["item_name"]; ?>&price=<?php echo $values["item_price"]; ?>">
		<tr>
			<td class="cart_product">
				<a href=""><img src="" alt=""></a><?php echo $num; ?>
			</td>
			<td class="cart_description">
				<h4><?php echo $values["item_id"];//$row['name']; ?></h4>
			</td>
			<td class="cart_description">
				<h4><?php echo $values["item_name"];//$row['name']; ?></h4>
			</td>
			<td class="cart_price">
				<p>RM<?php echo number_format($values['item_price'],2);//number_format($row['sellingPrice'],2); ?></p>
			</td>
			<td class="cart_quantity" style="width:13%;">
				<div class="input-group">
					<input type="number" class="form-control" value="<?php echo $values["quantity"]; ?>" name="quantity[<?php echo $values["item_id"]; ?>]" min="1" max="<?php echo $values["max_quantity"]; ?>">
					<span class="input-group-btn">
						<input class="btn btn-default" type="submit" value='Update'>
					</span>
				</div>
			</td>
			<td class="cart_total">
				<p>RM
				<?php echo number_format($subtotal,2); ?>
				</p>
			</td>
			<td>
				<a href="cart.php?action=remove&id=<?php echo $values["item_id"]; ?>&name=<?php echo $values["item_name"];?>" class="btn btn-danger" >Delete</a>
			</td>
		</tr>
    <?php  
		$num++;
			$total = $total + ($values["quantity"] * $values["item_price"]);  
			}
		}else{	
    ?>
		<tr>
			<td colspan="7" style="text-align:center;">Your cart is empty</td>
		</tr>
    <?php  
		}	
    ?>
	</tbody>  
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->
	
	<section id="do_action">
		<div class="container">
		
			<div class="heading">
				<h3></h3>
				<p></p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="total_area" hidden>
						
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Total <span>RM<?php echo number_format($totalprice,2); ?></span></li>
						</ul>
						<?php
							if(!empty($_SESSION["cart_items"])){
						?>
							<a href="checkout.php?id=<?php echo $_SESSION['user_id']; ?>">
								<input type="button" class="btn btn-default check_out" id="" name="" value="checkout">
							</a>
						<?php
							}else{
						?>
							<input type="button" class="btn btn-default check_out" onclick="alert('Your cart is empty');" id="" name="" value="checkout">
						<?php
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
</form>

	<footer id="footer"><!--Footer-->
		
		<div class="footer-widget">
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2016.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href=""></a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer--> 
	


    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>