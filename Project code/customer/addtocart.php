<?php
// start session 
session_start();
 
// get the product id
$id = isset($_GET['id']) ? $_GET['id'] : "";
$quantity = isset($_POST['quantity']) ? $_POST['quantity'] : 1;
$name = isset($_POST['product_name']) ? $_POST['product_name'] : "";
 
// make quantity a minimum of 1
$quantity=$quantity<=0 ? 1 : $quantity;
if(isset($_POST['addtocart'])&&isset($_SESSION['username'])){
	// add new item on array
	$cart_item=array(
		'quantity'=>$quantity,
		'item_name'=>$_POST['product_name'],
		'item_price'=>$_POST['price'],
		'item_id'=>$_POST['stock_id'],
		'max_quantity'=>$_POST['max_quantity'],
	);
	 
	/*
	 * check if the 'cart' session array was created
	 * if it is NOT, create the 'cart' session array
	 */
	if(!isset($_SESSION['cart_items'])){
		$_SESSION['cart_items'] = array(
			//'quantity'=>$quantity
		);
	}
	 
	// check if the item is in the array, if it is, do not add
	if(array_key_exists($id, $_SESSION['cart_items'])){
		// redirect to product list and tell the user it was added to cart
		header('Location: productDetails.php?action=added&id=' . $id . '&name=' . $name);
	}
	 
	// else, add the item to the array
	else{
		$_SESSION['cart_items'][$id]=$cart_item;
	 
		// redirect to product list and tell the user it was added to cart
		header('Location: productDetails.php?id=' . $id . '&name=' . $name);
	}
}else{
	echo "<script>";
	echo "alert('Please login.');";
	echo "window.location='../login.php';";
	echo "</script>";
}

if(isset($_POST['update_cart'])){
	foreach($_SESSION["cart_items"] as $keys => $values){  
		if($values["item_id"] == $_GET["id"])  
		{
			// remove the item from the array
			unset($_SESSION['cart_items'][$keys]);
			
			// add the item with updated quantity
			$_SESSION['cart_items'][$id]=array(
				'quantity'=>$_POST['quantity'],
				'item_name'=>$_POST['product_name'],
				'item_price'=>$_POST['price'],
				'item_id'=>$_POST['stock_id'],
				'max_quantity'=>$_POST['max_quantity'],
			);
			header('Location: productDetails.php?action=updated&id=' . $id . '&name=' . $name);
		}
	}
}
echo '<pre>';
print_r($_SESSION["cart_items"]);
echo '</pre>';
?>