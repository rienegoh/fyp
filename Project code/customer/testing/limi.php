<?php
	include('../../assets/config/config.php');
	
	
		

	
	/* ---------------------------------------Pagination-------------------------------------------------*/
	$tbl_name="stock";		//table name
	
	$query = "SELECT COUNT(*) as num FROM $tbl_name";
	$total_pages = mysql_fetch_array(mysql_query($query));
	$total_pages = $total_pages['num'];
	
	/* Setup vars for query. */
	$targetpage = "limi.php"; 	//file name  (the name of this file)
	
	if(isset($_POST['select_limit'])){
		$sl = $_POST['select_limit'];
		$limit = preg_replace("#[^0-9]#","",$sl);
	}else{
		$limit = 10;
	}
	
	//$limit = 5; 								//how many items to show per page
	$page = $_GET['page'];
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0
	
	/* Get data. */
	$sql = "SELECT * FROM stock INNER JOIN category ON stock.category_prefix = category.category_shortform LIMIT $start, $limit";
	$result = mysql_query($sql);
	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	/* ---------------------------------------Pagination End-------------------------------------------------*/			
?>
		<script type="text/javascript">

		</script>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Inventory Manage</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
							<div class="panel panel-default" style="margin-left:2.5%;width:145%;">
								<div class="panel-heading">
									Inventory Detail
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<form method="post" action="inventory.php">
										<button type="submit" name="delete" style="float:left;margin:0 0.5% 2% 0;" class="btn btn-sm btn-default" onclick="return confirm('Are you sure?');" value="delete"><i class="glyphicon glyphicon-remove"></i> Delete</button>
										<br/>
										<table id="datatables" class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th style="width:4%;">
														<div class="checkbox">
																<input type="checkbox" id="checkall" onchange="checkall()">
														</div>
													</th>
													<th style="width:8%;">ID</th>
													<th style="width:20%;">Name</th>
													<th style="width:5%;">Quantity</th>
													<th style="width:15%;">Company Price</th>
													<th style="width:15%;">Selling Price</th>
													<th style="width:20%;">Category</th>
													<th style="width:5%;">Edit</th>
												</tr>
											</thead>
<?php
	//$query = mysql_query("SELECT * FROM stock")or die(mysql_error());
	while($row = mysql_fetch_assoc($result)){
?>
<tbody id="tb">
	<tr>
		<td><?php echo $row['ID']; ?></td>
		<td><?php echo $row['name']; ?></td>
		<td><?php echo $row['quantity']; ?></td>
		<td><?php echo 'RM '.number_format($row['companyPrice'],2); ?></td>
		<td><?php echo 'RM '.number_format($row['sellingPrice'],2); ?></td>
		<td><?php echo $row['categoryName']; ?></td>
		<td><a href="editstock.php?edit=<?php echo $row['ID'];?>" class="btn btn-primary"><i class="glyphicon glyphicon-edit">&nbsp;</i>Edit</a></td>
		<!--<td><a href="test.php?delete=<?php echo $row['ID'];?>&name=<?php echo $row['name'];?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove">&nbsp;</i>Delete</a></td>-->
	</tr>
</tbody>
<?php
	}
?>
										</table>
											<?php
											$check = mysql_query('SELECT * FROM stock');
											$total = mysql_num_rows($check);
											$end = $page * $limit;
											$first= (($page - 1) * $limit) + 1;
											$messege = '<span style="float:left;margin-top:0;">
												Showing '.$first.' to '.$end.' of '.$end.' entries '.'(filtered from '.$total.' total entries)
												</span>';
											//echo $messege;
											
											?>
											<span style="float:left;height:25px;">	
											<?php
												include 'pagination.php';
											?>
											</span>
										</form>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    